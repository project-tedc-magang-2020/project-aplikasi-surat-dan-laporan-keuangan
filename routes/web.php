<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Datamaster;
use App\Http\Controllers\Surat;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\DashboardSuratController;
use App\Http\Controllers\Datamaster\DashboardController;
use App\Http\Controllers\Datamaster\KaryawanController;
use App\Http\Controllers\Datamaster\JabatanController;
use App\Http\Controllers\Datamaster\TipeSuratController;
use App\Http\Controllers\Datamaster\ClientController;
use App\Http\Controllers\Surat\SuratmenuController;
use App\Http\Controllers\Surat\SuratinvoicesController;
use App\Http\Controllers\Surat\DatasuratController;
use App\Http\Controllers\Surat\DatainvoiceController;
use App\Http\Controllers\Surat\DatabastController;
use App\Http\Controllers\Surat\IteminvoicesController;
use App\Http\Controllers\Surat\InvoicesController;
use App\Http\Controllers\Surat\ProjectController;
use App\Http\Controllers\Surat\BastController;
use App\Http\Controllers\Keuangan\HomeKeuanganController;
use App\Http\Controllers\Keuangan\BukuBesarController;
use App\Http\Controllers\Keuangan\BukuPembantuController;
use App\Http\Controllers\Keuangan\NeracaLajurController;
use App\Http\Controllers\Keuangan\KodeAkunController;
use App\Http\Controllers\Keuangan\KodeBantuController;
use App\Http\Controllers\Keuangan\JurnalUmumController;
use App\Http\Controllers\Keuangan\InputJurnalController;
use App\Http\Controllers\Keuangan\LabaRugiController;
use App\Http\Controllers\Keuangan\NeracaController;
use App\Http\Controllers\Keuangan\PerubahanModalController;
use App\Http\Controllers\Keuangan\PreviewJurnalController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [LoginController::class, 'login'])->name('login');
Route::post('actionlogin', [LoginController::class, 'actionlogin'])->name('actionlogin');

Route::get('home', [HomeController::class, 'index'])->name('home')->middleware('auth');
Route::get('actionlogout', [LoginController::class, 'actionlogout'])->name('actionlogout')->middleware('auth');
Route::get('registration', [RegistrationController::class, 'registration'])->name('register-user');
Route::post('custom-registration', [RegistrationController::class, 'customRegistration'])->name('register.custom');
Route::get('dashboardsurat', [DashboardSuratController::class, 'index'])->name('dashboardsurat')->middleware('auth');
Route::prefix('datamaster')->group(function(){
	Route::get('/dashboard', [App\Http\Controllers\Datamaster\DashboardController::class, 'index'])->name('datamaster.dashboard');

	Route::get('/karyawan', [Datamaster\KaryawanController::class, 'index'])->name('datamaster.karyawan');
	Route::get('/tambahdatakaryawan', [Datamaster\KaryawanController::class, 'create'])->name('datamaster.tambahdatakaryawan');
	Route::post('/karyawan/store', [Datamaster\KaryawanController::class, 'store'])->name('datamaster.storekaryawan');
	Route::get('/karyawan/edit/{id_karyawan}', [Datamaster\KaryawanController::class, 'edit'])->name('datamaster.editdatakaryawan');
	Route::post('/karyawan/update/{id_karyawan}', [Datamaster\KaryawanController::class, 'update'])->name('datamaster.updatedatakaryawan');
	Route::get('/karyawan/{id_karyawan}', [Datamaster\KaryawanController::class, 'destroy'])->name('datamaster.destroykaryawan');

	Route::get('/jabatan', [Datamaster\JabatanController::class, 'index'])->name('datamaster.jabatan');
	Route::get('/tambahdatajabatan', [Datamaster\JabatanController::class, 'create'])->name('datamaster.tambahdatajabatan');
	Route::post('/jabatan/store', [Datamaster\JabatanController::class, 'store'])->name('datamaster.storejabatan');
	Route::get('/jabatan/edit/{id_jabatan}', [Datamaster\JabatanController::class, 'edit'])->name('datamaster.editjabatan');
	Route::post('/jabatan/update/{id_jabatan}', [Datamaster\JabatanController::class, 'update'])->name('datamaster.updatedatajabatan');
	Route::get('/jabatan/{id_jabatan}', [Datamaster\JabatanController::class, 'destroy'])->name('datamaster.destroyjabatan');

	Route::get('/tipesurat', [Datamaster\TipeSuratController::class, 'index'])->name('datamaster.tipesurat');
	Route::get('/tambahdatatipesurat', [Datamaster\TipeSuratController::class, 'create'])->name('datamaster.tambahdatatipesurat');
	Route::post('/tipesurat/store', [Datamaster\TipeSuratController::class, 'store'])->name('datamaster.storetipesurat');
	Route::get('/tipesurat/edit/{id_tipe_surat}', [Datamaster\TipeSuratController::class, 'edit'])->name('datamaster.editdatatipesurat');
	Route::post('/tipesurat/update/{id_tipe_surat}', [Datamaster\TipeSuratController::class, 'update'])->name('datamaster.updatedatatipesurat');
	Route::get('/tipesurat/{id_tipe_surat}', [Datamaster\TipeSuratController::class, 'destroy'])->name('datamaster.destroytipesurat');

	Route::get('/client', [App\Http\Controllers\Datamaster\ClientController::class, 'index'])->name('datamaster.client');
	Route::get('/client/create', [App\Http\Controllers\Datamaster\ClientController::class, 'create'])->name('datamaster.createclient');
    Route::post('/client/store', [App\Http\Controllers\Datamaster\ClientController::class, 'store'])->name('datamaster.storeclient');
    Route::post('/client/update/{id_client}', [App\Http\Controllers\Datamaster\ClientController::class, 'update'])->name('datamaster.updateclient');
    Route::get('/client/edit/{id_client}', [App\Http\Controllers\Datamaster\ClientController::class, 'edit'])->name('datamaster.editclient');
    Route::get('/client/{id_client}', [App\Http\Controllers\Datamaster\ClientController::class, 'destroy'])->name('datamaster.destroyclient');
});

Route::prefix('surat')->group(function(){
	Route::get('/dashboardsurat', [SuratmenuController::class, 'index'])->name('surat.dashboard');
	Route::get('/createsurat/invoice', [SuratinvoicesController::class, 'index'])->name('create.invoice');
	Route::post('/storesuratinvoice', [SuratinvoicesController::class, 'store'])->name('invoice.store');
	Route::get('/cetaksuratinvoice/{id_surat}', [SuratinvoicesController::class, 'show'])->name('invoice.cetak');

	Route::get('/datasurat', [DatasuratController::class, 'index'])->name('surat.datasurat');


	Route::get('/datasurat/invoice', [DatainvoiceController::class, 'index'])->name('invoice.datasurat');
	Route::post('/datasurat/updateinvoice/{id_surat}', [DatainvoiceController::class, 'update'])->name('invoice.updatedatasurat');
	Route::get('/datasurat/editinvoice/{id_surat}', [DatainvoiceController::class, 'edit'])->name('invoice.editdatasurat');
    Route::get('/datasurat/printinvoice/{id_surat}', [DatainvoiceController::class, 'show'])->name('invoice.printdatasurat');
    Route::get('/datasuratinvoice/{id_surat}', [DatainvoiceController::class, 'destroy'])->name('invoice.destroydatasurat');


    Route::get('/datasurat/bast', [DatabastController::class, 'index'])->name('bast.datasurat');
	Route::post('/datasurat/updatebast/{id_surat}', [DatabastController::class, 'update'])->name('bast.updatedatasurat');
	Route::get('/datasurat/editbast/{id_surat}', [DatabastController::class, 'edit'])->name('bast.editdatasurat');
    Route::get('/datasurat/printbast/{id_surat}', [DatabastController::class, 'show'])->name('bast.printdatasurat');
    Route::get('/datasuratbast/{id_surat}', [DatabastController::class, 'destroy'])->name('bast.destroydatasurat');


    Route::get('/iteminvoices', [IteminvoicesController::class, 'index'])->name('surat.iteminvoices');
    Route::post('/iteminvoices/store', [IteminvoicesController::class, 'store'])->name('surat.storeiteminvoices');
    Route::post('/iteminvoices/update/{id_item}', [IteminvoicesController::class, 'update'])->name('surat.updateiteminvoices');
    Route::get('/iteminvoices/edit/{id_item}', [IteminvoicesController::class, 'edit'])->name('surat.edititeminvoices');
    Route::get('/iteminvoices/{id_item}', [IteminvoicesController::class, 'destroy'])->name('surat.destroyiteminvoices');

    Route::get('/invoices', [InvoicesController::class, 'index'])->name('surat.invoices');
    Route::post('/invoices/store', [InvoicesController::class, 'store'])->name('surat.storeinvoices');
    Route::post('/invoices/update/{id_invoice}', [InvoicesController::class, 'update'])->name('surat.updateinvoices');
    Route::get('/invoices/edit/{id_invoice}', [InvoicesController::class, 'edit'])->name('surat.editinvoices');
    Route::get('/invoices/{id_invoice}', [InvoicesController::class, 'destroy'])->name('surat.destroyinvoices');

    // PROJECT
	Route::get('/project', [App\Http\Controllers\Surat\ProjectController::class, 'index'])->name('surat.project');
	Route::get('/project/create', [App\Http\Controllers\Surat\ProjectController::class, 'create'])->name('surat.tambahdataproject');
	Route::post('/project/store', [App\Http\Controllers\Surat\ProjectController::class, 'store'])->name('surat.storeproject');
	Route::post('/project/update/{id_project}', [App\Http\Controllers\Surat\ProjectController::class, 'update'])->name('surat.updatedataproject');
	Route::get('/project/edit/{id_project}', [App\Http\Controllers\Surat\ProjectController::class, 'edit'])->name('surat.editdataproject');
	Route::get('/project/{id_project}', [App\Http\Controllers\Surat\ProjectController::class, 'destroy'])->name('surat.destroyproject');
    
	// BAST
	Route::get('/createsurat/bast', [BastController::class, 'index'])->name('create.bast');
	Route::post('/storesuratbast', [BastController::class, 'store'])->name('bast.storebast');
	Route::get('/cetaksuratbast/{id_surat}', [BastController::class, 'edit'])->name('bast.cetakbast');
});

Route::prefix('keuangan')->group(function(){

	Route::get('/home', [HomeKeuanganController::class, 'index'])->name('keuangan.home');
	Route::get('/kodeakun', [KodeAkunController::class, 'index'])->name('keuangan.kodeakun');
	Route::get('/perubahanmodal', [PerubahanModalController::class, 'index'])->name('keuangan.perubahanmodal');

	Route::get('/bukubesar', [BukuBesarController::class, 'index'])->name('keuangan.bukubesar');
	Route::get('/bukupembantu', [BukuPembantuController::class, 'index'])->name('keuangan.bukupembantu');
	Route::get('/labarugi', [LabaRugiController::class, 'index'])->name('keuangan.labarugi');
	Route::post('/laba_rugi/update/{id}', [LabaRugiController::class, 'update'])->name('keuangan.updatelabarugi');
	Route::get('/neraca', [NeracaController::class, 'index'])->name('keuangan.neraca');
	Route::get('/neracalajur', [NeracaLajurController::class, 'index'])->name('keuangan.neracalajur');

	//KODE AKUN
	Route::get('/kodeakun', [KodeAkunController::class, 'index'])->name('keuangan.kodeakun');
	Route::get('/tambahdatakodeakun', [KodeAkunController::class, 'create'])->name('keuangan.tambahdatakodeakun');
	Route::post('/kodeakun/store', [KodeAkunController::class, 'store'])->name('kodeakun.storedataakun');
	Route::get('/kodeakun/edit/{kode_akun}', [KodeAkunController::class, 'edit'])->name('kodeakun.editdatakodeakun');
	Route::post('/kodeakun/update/{kode_akun}', [KodeAkunController::class, 'update'])->name('kodeakun.updatedatakodeakun');
	Route::get('/kodeakun/{kode_akun}', [KodeAkunController::class, 'destroy'])->name('kodeakun.destroykodeakun');

	//KODE AKUN BANTU
	Route::get('/kodeakunbantu', [KodeBantuController::class, 'index'])->name('keuangan.kodeakunbantu');
	Route::get('/tambahdatakodeakunbantu', [KodeBantuController::class, 'create'])->name('keuangan.tambahdatakodeakunbantu');
	Route::post('/kodeakunbantu/store', [KodeBantuController::class, 'store'])->name('kodebantu.storedataakunbantu');
	Route::get('/kodeakunbantu/edit/{kode_akun_bantu}', [KodeBantuController::class, 'edit'])->name('kodebantu.editdatakodeakunbantu');
	Route::post('/kodeakunbantu/update/{kode_akun_bantu}', [KodeBantuController::class, 'update'])->name('kodebantu.updatedatakodeakunbantu');
	Route::get('/kodeakunbantu/{kode_akun_bantu}', [KodeBantuController::class, 'destroy'])->name('kodebantu.destroykodeakunbantu');

	//Input Jurnal
	Route::get('/inputjurnal', [InputJurnalController::class, 'index'])->name('keuangan.inputjurnal');
	Route::post('/inputjurnal/store', [InputJurnalController::class, 'store'])->name('keuangan.storeinputjurnal');

	//Preview Jurnal
	Route::get('/previewjurnal', [PreviewJurnalController::class, 'index'])->name('keuangan.previewjurnal');
	Route::get('/listbukubesar/{kode_akun}', [BukuBesarController::class, 'listBukuBesar'])->name('keuangan.listbukubesar');
	// Route::get('/bulanbukubesar/{tanggal}', [BukuBesarController::class, 'bulanBukuBesar'])->name('keuangan.bulanbukubesar');
	Route::get('/bulanbukubesar/{tanggal}', [BukuBesarController::class, 'bulanBukuBesar'])->name('keuangan.bulanbukubesar');

	Route::get('/listbukubantu/{kode_akun_bantu}', [BukuPembantuController::class, 'listBukuBantu'])->name('keuangan.listbukubantu');
	Route::get('/bulanbukubantu/{tanggal}', [BukuPembantuController::class, 'bulanBukuBantu'])->name('keuangan.bulanbukubantu');

	//Jurnal Umum
	Route::get('/jurnalumum', [JurnalUmumController::class, 'index'])->name('keuangan.jurnalumum');
	Route::post('/jurnalumum/store', [JurnalUmumController::class, 'store'])->name('keuangan.storejurnalumum');
	Route::post('/jurnalumum/update/{id_jurnal_umum}', [JurnalUmumController::class, 'update'])->name('keuangan.updatejurnalumum');
	Route::get('/jurnalumum/{id_jurnal_umum}', [JurnalUmumController::class, 'destroy'])->name('keuangan.destroyjurnalumum');
	Route::get('/jurnalumum/preview/{id_jurnal_umum}', [JurnalUmumController::class, 'show'])->name('keuangan.previewjurnalumum');	

});