@extends('layouts.home.app')
@section('content')



<!-- Begin Page Content -->
    <div class="container">
        <h2 class="h4 mb-0 text-gray-800">Menu</h2>
    </div>
    <br>
    <br />
    <br />
<section>
  <center>
  <div class="row">

    <!-- Earnings (Monthly) Card Example -->
      <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-1" style="background-image: linear-gradient(315deg, #f7b42c 0%, #fc575e 74%); width: 20rem;">
          <div class="card-body" style="height: 16rem;">
            <div class="container">
              <center>
                <img src="{{URL::asset('images/menu1.png')}}" width="170" height="170">
                <div class="card-body">
                  <a href="{{ route('surat.dashboard') }}" class="btn" role="button" style="background-color: #1f9eda; color: white;">Surat</a>
                </div>
              </center>
            </div>
          </div>
        </div>
      </div>

      <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-4 col-md-6 mb-4">
          <div class="card border-left-primary shadow h-100 py-1" style="background-image: linear-gradient(315deg, #f7b42c 0%, #fc575e 74%); width: 20rem;">
            <div class="card-body" style="height: 16rem;">
              <div class="container">
                <center>
                  <img src="{{URL::asset('images/menu2.png')}}" width="170" height="170">
                  <div class="card-body">
                    <button href="#" class="btn" style="background-color: #1f9eda; color: white;">Laporan Keuangan</button>
                  </div>
                </center>
              </div>
            </div>
          </div>
        </div>

      <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-4 col-md-6 mb-4">
          <div class="card border-left-primary shadow h-100 py-1" style="background-image: linear-gradient(315deg, #f7b42c 0%, #fc575e 74%); width: 20rem;">
            <div class="card-body" style="height: 16rem;">
              <div class="container">
                <center>
                  <img src="{{URL::asset('images/menu3.png')}}" width="170" height="170">
                  <div class="card-body">
                    <a href="{{ route('datamaster.dashboard') }}" class="btn" role="button" style="background-color: #1f9eda; color: white;">Data Master</a>
                  </div>
                </center>
              </div>
            </div>
          </div>
        </div>
      </div>
      </center>
      </section>
      <br><br><br>

            
@endsection