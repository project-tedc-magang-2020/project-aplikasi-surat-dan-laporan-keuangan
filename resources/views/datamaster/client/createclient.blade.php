@extends('layouts.home.app')
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col">
			<div class="container">
				<a href="/datamaster/client">
					<button type="button" class="btn btn-secondary" style="float: right;">
  				<i class="bi bi-arrow-counterclockwise" width="20" height="20" fill="currentColor"></i>
              </button>
				</a>
			<br>
			<br>
			<div class="container">
			<h3><center>Tambah Data Client</center></h3>
			<br />
			
			<br />
			<form action="{{ route('datamaster.storeclient') }}" method="POST">
				@csrf
				<!--<div class="form-group">
					<label>ID</label>
					<input type="text" name="id_client" class="form-control" placeholder="Id Client">
				</div>-->
				<div class="form-group">
					<label>Nomor Client</label>
					<input type="text" name="nomor_client" value ="{{ 'CL-'.$kd}}" class="form-control" readonly>
				</div>
				<div class="form-group">
					<label>Nama Client</label>
					<input type="text" name="nama_client" class="form-control" placeholder="Nama Client" required>
				</div>
				<div class="form-group">
					<label>Alamat</label>
					<textarea type="textarea" name="alamat" class="form-control" rows="3" placeholder="Alamat" required></textarea>
				</div>
				<div class="form-group">
					<label>Telepon</label>
					<input type="number" name="telepon" class="form-control" placeholder="Telepon" required= "tidak boleh kosong">
				</div>
                <div class="form-group">
					<label>Email</label>
					<input type="text" name="email" class="form-control" placeholder="Email" required= "tidak boleh kosong">
				</div>
                <div class="form-group">
					<label>Penanggung Jawab 1</label>
					<input type="text" name="penanggung_jawab1" class="form-control" placeholder="Penanggung Jawab 1" required= "tidak boleh kosong">
				</div>
                <div class="form-group">
					<label>Penanggung Jawab Jabatan 1</label>
					<input type="text" name="penanggung_jawab_jabatan1" class="form-control" placeholder="Penanggung Jawab jabatan 1" required= "tidak boleh kosong">
				</div>
                <div class="form-group">
					<label>Penanggung Jawab 2</label>
					<input type="text" name="penanggung_jawab2" class="form-control" placeholder="Penanggung Jawab 2" required= "tidak boleh kosong">
				</div>
                <div class="form-group">
					<label>Penanggung Jawab Jabatan 2</label>
					<input type="text" name="penanggung_jawab_jabatan2" class="form-control" placeholder="Penanggung Jawab jabatan 2" required= "tidak boleh kosong">
				</div>
                <div class="form-group">
					<label>Penanggung Jawab 3</label>
					<input type="text" name="penanggung_jawab3" class="form-control" placeholder="Penanggung Jawab 3" required= "tidak boleh kosong">
				</div>
                <div class="form-group">
					<label>Penanggung Jawab Jabatan 3</label>
					<input type="text" name="penanggung_jawab_jabatan3" class="form-control" placeholder="Penanggung Jawab jabatan 3" required= "tidak boleh kosong">
				</div>
				<br />
				<center>
					<button type="submit" class="btn btn-primary">Simpan</button>
					<a class="btn btn-danger" href="/datamaster/client" role="button">Cancel</a>
				</center>
			</form>
			<br>
			</div>
			</div>
		</div>
	</div>
</div>
@endsection