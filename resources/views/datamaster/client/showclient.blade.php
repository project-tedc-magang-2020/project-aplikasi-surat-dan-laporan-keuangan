@extends('layouts.home.app')
@section('content')
<div class="row">
                    <div class="col-lg-12">
                    <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Nomor Client</th>
                        <td>{{ $cli->nomor_client }}</td>
                        </tr>
                        <tr>
                        <th>Nama Client</th>
                        <td>{{ $cli->nama_client }}</td>
                        </tr>
                        <tr>
                        <th>Alamat</th>
                        <td>{{ $cli->alamat}}</td>
                        </tr>
                        <tr>
                        <th>Telepon</th>
                        <td>{{ $cli->telepon }}</td>
                        </tr>
                        <tr>
                        <th>Email</th>
                        <td>{{ $cli->email }}</td>
                        </tr>
                        <tr>
                        <th>penanggung jawab1</th>
                        <td>{{ $cli->penanggung_jawab1 }}</td>
                        </tr>
                        <tr>
                        <th>penanggung jawab jabatan 1</th>
                        <td>{{ $cli->penanggung_jawab_jabatan1 }}</td>
                        </tr>
                        <tr>
                        <th>penanggung jawab2</th>
                        <td>{{ $cli->penanggung_jawab2 }}</td>
                        </tr>
                        <tr>
                        <th>penanggung jawab jabatan 2</th>
                        <td>{{ $cli->penanggung_jawab_jabatan2 }}</td>
                        </tr>
                        <tr>
                        <th>penanggung jawab3</th>
                        <td>{{ $cli->penanggung_jawab3 }}</td>
                        </tr>
                        <tr>
                        <th>penanggung jawab jabatan 3</th>
                        <td>{{ $cli->penanggung_jawab_jabatan3 }}</td>
                        </tr>
                    </thead>
                    </table>
                    </div>
                    </div>
@endsection
