@extends('layouts.home.app')
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col">
			<div class="container">
			<h2><center>Edit Data Client</center></h2>
			<br />
			
			<br />
			@foreach ($clients as $pe)
			<form action="{{ route('datamaster.updateclient') }}" method="POST">
				{{ csrf_field() }}
				<div class="form-group">
					<label>Id Client</label>
					<input type="text" name="id_client" class="form-control" required="required" disabled value="{{ $pe->id_client }}">
				</div>
				<div class="form-group">
					<label>Nomor Client</label>
					<input type="text" name="nomor_client" class="form-control" required="required" value="{{ $pe->nomor_client }}">
				</div>
				<div class="form-group">
					<label>Nama Client</label>
					<input type="text" name="nama_client" class="form-control" required="required" value="{{ $pe->nama_client}}">
				</div>
				<div class="form-group">
					<label>Alamat</label>
					<input type="textarea" name="alamat" class="form-control" required="required" value="{{ $pe->alamat}}">
				</div>
				<div class="form-group">
					<label>Telepon</label>
					<input type="text" name="telepon" class="form-control" required="required" value="{{ $pe->telepon}}">
				</div>
                <div class="form-group">
					<label>Email</label>
					<input type="text" name="email" class="form-control" required="required" value="{{ $pe->email }}">
				</div>
               
                <div class="form-group">
					<label>Penanggung Jawab 1</label>
					<input type="text" name="penanggung_jawab1" class="form-control" required="required" value="{{ $pe->penanggung_jawab1 }}">
				</div>
                <div class="form-group">
					<label>Penanggung Jawab Jabatan 1</label>
					<input type="text" name="penanggung_jawab_jabatan1" class="form-control" required="required" value="{{ $pe->penanggung_jawab_jabatan1 }}">
				</div>
                <div class="form-group">
					<label>Penanggung Jawab 2</label>
					<input type="text" name="penanggung_jawab2" class="form-control" required="required" value="{{ $pe->penanggung_jawab2 }}">
				</div>
                <div class="form-group">
					<label>Penanggung Jawab jabatan 2</label>
					<input type="text" name="penanggung_jawab_jabatan2" class="form-control" required="required" value="{{ $pe->penanggung_jawab_jabatan2 }}">
				</div>
                <div class="form-group">
					<label>Penanggung Jawab 3</label>
					<input type="text" name="penanggung_jawab3" class="form-control" required="required" value="{{ $pe->penanggung_jawab3 }}">
				</div>
                <div class="form-group">
					<label>Penanggung Jawab jabatan 3</label>
					<input type="text" name="penanggung_jawab_jabatan3" class="form-control" required="required" value="{{ $pe->penanggung_jawab_jabatan3 }}">
				</div>
				<br />
				<center><button type="submit" class="btn btn-primary">Simpan</button></center>
			</form>
			@endforeach
			</div>
		</div>
	</div>
</div>

@endsection