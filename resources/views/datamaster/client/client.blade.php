@extends('layouts.home.app')
@section('content')


<!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="container">
                    <h2 class="h3 mb-2 text-gray-800">Clients</h2>
                    <br />
                @include('layouts.messages')
                <br />
                    <a data-toggle="modal" data-target="#addModal">
                        <button class="bi bi-plus-circle-fill btn" style="background-image: linear-gradient(315deg, #f7b42c 0%, #fc575e 74%); color:white;"> Tambah Data</button>
                    </a>
                    </div>
                    <br/>

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4" style="background-image: linear-gradient(315deg, #f7b42c 0%, #fc575e 74%); width: 100%;">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" style="background-color: white; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="vertical-align: middle; text-align: center; width: 10%;">No.</th>
                                            <th style="vertical-align: middle; text-align: center; width: 25%;">Nama Client</th>
                                            <th style="vertical-align: middle; text-align: center; width: 25%;">Alamat</th>
                                            <th style="vertical-align: middle; text-align: center; width: 20%;">Telepon</th>
                                            <th style="vertical-align: middle; text-align: center; width: 20%;">Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                    </tfoot>
                                    
                                    <tbody>
                                    <?php $no=1; ?>
                                    @foreach ($clients as $pe)
                                        <tr>
                                            <td style="vertical-align: middle; text-align: center;">{{$no++}}.</td>
                                            <td style="vertical-align: middle; text-align: center;">{{ $pe->nama_client }}</td>
                                            <td style="vertical-align: middle; text-align: center;">{{ $pe->alamat }}</td>
                                            <td style="vertical-align: middle; text-align: center;">{{ $pe->telepon }}</td>
                                            <td style="text-align: center;">
                                                <div class="container mb-0">
                                                <button data-toggle="modal" data-target="#editModal{{ $pe->id_client }}" class="bi bi-pencil-square editbtn btn btn-warning col-xl-3 col-md-4 mb-2" style="font-size: 1.3rem; color:white;" role="button"></button>
                                                <button data-toggle="modal" data-target="#my-modal{{ $pe->id_client }}" class="bi bi-trash-fill btn btn-danger col-xl-3 col-md-4 mb-2" style="font-size: 1.2rem; color:white;" role="button"></button>
                                                <button class="bi bi-eye-fill detailbtn btn btn-info col-xl-3 col-md-4 mb-2" data-toggle="modal" data-target="#myModal{{ $pe->id_client }}" style="font-size: 1.3rem; color:white;" role="button"></button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->


            <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
            <!-- End of Main Content -->


    @foreach ($clients as $pe)
    <div id="my-modal{{ $pe->id_client }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content border-0">   
                <div class="modal-body p-0">
                    <div class="card border-0 p-sm-3 p-2 justify-content-center">
                        <div class="card-header pb-0 bg-white border-0 "><div class="row"><div class="col ml-auto"><button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div> </div>
                        <p class="font-weight-bold mb-2"> Are you sure you wanna delete this ?</p><p class="text-muted "> These changes will be visible on your portal and the data will be deleted.</p>     </div>
                        <div class="card-body px-sm-4 mb-2 pt-1 pb-0"> 
                            <div class="row justify-content-end no-gutters"><div class="col-auto"><button type="button" class="btn btn-light text-muted" data-dismiss="modal">Cancel</button><a class="btn btn-danger px-4" href="/datamaster/client/{{ $pe->id_client }}" role="button">Delete</a></div><div class="col-auto"></div></div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
    @endforeach

    @foreach ($clients as $pe)
    <!-- Modal Detail -->
    <div id="myModal{{ $pe->id_client }}" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
            <!-- konten modal-->
            <div class="modal-content" style="background-image: linear-gradient(315deg, #f7b42c 0%, #fc575e 74%);">
                <!-- heading modal -->
                <div class="modal-header text-white">
                <h4>Detail Data Client</h4>
                    <button type="button" class="close" style="color: white;" data-dismiss="modal">&times;</button>
                    <!-- <h4 class="modal-title">Ini adalah heading dari Modal</h4> -->
                </div>
                <!-- body modal -->
                <div class="modal-body">
                    <div class="row">
                    <div class="col-lg-12">
                    <table class="table table-bordered no-margin" style="background-color: white;">
                    <thead>
                    <tr>
                        <th>Nomor Client</th>
                        <td>{{ $pe->nomor_client }}</td>
                        </tr>
                        <tr>
                        <th>Nama Client</th>
                        <td>{{ $pe->nama_client }}</td>
                        </tr>
                        <tr>
                        <th>Alamat</th>
                        <td>{{ $pe->alamat}}</td>
                        </tr>
                        <tr>
                        <th>Telepon</th>
                        <td>{{ $pe->telepon }}</td>
                        </tr>
                        <tr>
                        <th>Email</th>
                        <td>{{ $pe->email }}</td>
                        </tr>
                        <tr>
                        <th>Jabatan</th>
                        <td>{{ $pe->jabatan }}</td>
                        </tr>
                        <tr>
                        <th>Perusahaan</th>
                        <td>{{ $pe->perusahaan }}</td>
                        </tr>
                        <tr>
                        <th>Penanggung Jawab 1</th>
                        <td>{{ $pe->penanggung_jawab1 }}</td>
                        </tr>
                        <tr>
                        <th>Penanggung Jawab Jabatan 1</th>
                        <td>{{ $pe->penanggung_jawab_jabatan1 }}</td>
                        </tr>
                        <tr>
                        <th>Penanggung Jawab 2</th>
                        <td>{{ $pe->penanggung_jawab2 }}</td>
                        </tr>
                        <tr>
                        <th>Penanggung Jawab Jabatan 2</th>
                        <td>{{ $pe->penanggung_jawab_jabatan2 }}</td>
                        </tr>
                        <tr>
                        <th>Penanggung Jawab 3</th>
                        <td>{{ $pe->penanggung_jawab3 }}</td>
                        </tr>
                        <tr>
                        <th>Penanggung Jawab Jabatan 3</th>
                        <td>{{ $pe->penanggung_jawab_jabatan3 }}</td>
                        </tr>
                    </thead>
                    </table>
                 </div>
                </div>
            </div>
                <!-- footer modal -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    @endforeach
   
   @foreach ($clients as $pe)
    <div class="modal fade" id="editModal{{ $pe->id_client }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="background-image: linear-gradient(315deg, #f7b42c 0%, #fc575e 74%);">
                <div class="modal-header text-white">
                    <h4 class="modal-title" id="exampleModalLabel">Edit Data</h4>
                    <button type="button" class="close" style="color: white;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                <form action="{{ url('/datamaster/client/update',  $pe->id_client) }}" method="POST" id="editform">
                {!! csrf_field() !!}
                <div class="form-group">
                    <label>Nomor Client</label>
                    <input type="text" name="nomor_client" class="form-control" required="required" readonly value="{{ $pe->nomor_client }}">
                </div>
                <div class="form-group">
                    <label>Nama Client</label>
                    <input type="text" name="nama_client" class="form-control" required="required" value="{{ $pe->nama_client}}">
                </div>
                <div class="form-group">
                    <label>Alamat</label>
                    <input type="textarea" name="alamat" class="form-control" required="required" value="{{ $pe->alamat}}">
                </div>
                <div class="form-group">
                    <label>Telepon</label>
                    <input type="text" name="telepon" class="form-control" required="required" value="{{ $pe->telepon}}">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" name="email" class="form-control" required="required" value="{{ $pe->email }}">
                </div>
                <div class="form-group">
                    <label>Jabatan</label>
                    <input type="text" name="jabatan" class="form-control" required="required" value="{{ $pe->jabatan}}">
                </div>
                <div class="form-group">
                    <label>Perusahaan</label>
                    <input type="text" name="perusahaan" class="form-control" required="required" value="{{ $pe->perusahaan}}">
                </div>
                <div class="form-group">
                    <label>Penanggung Jawab 1</label>
                    <input type="text" name="penanggung_jawab1" class="form-control" required="required" value="{{ $pe->penanggung_jawab1 }}">
                </div>
                <div class="form-group">
                    <label>Penanggung Jawab Jabatan 1</label>
                    <input type="text" name="penanggung_jawab_jabatan1" class="form-control" required="required" value="{{ $pe->penanggung_jawab_jabatan1 }}">
                </div>
                <div class="form-group">
                    <label>Penanggung Jawab 2</label>
                    <input type="text" name="penanggung_jawab2" class="form-control" required="required" value="{{ $pe->penanggung_jawab2 }}">
                </div>
                <div class="form-group">
                    <label>Penanggung Jawab jabatan 2</label>
                    <input type="text" name="penanggung_jawab_jabatan2" class="form-control" required="required" value="{{ $pe->penanggung_jawab_jabatan2 }}">
                </div>
                <div class="form-group">
                    <label>Penanggung Jawab 3</label>
                    <input type="text" name="penanggung_jawab3" class="form-control" required="required" value="{{ $pe->penanggung_jawab3 }}">
                </div>
                <div class="form-group">
                    <label>Penanggung Jawab jabatan 3</label>
                    <input type="text" name="penanggung_jawab_jabatan3" class="form-control" required="required" value="{{ $pe->penanggung_jawab_jabatan3 }}">
                </div>
                
                <br />
                <div class="modal-footer">
                <center><button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button></center>
                <center><button type="submit" class="btn btn-primary">Simpan</button></center>
                </div>
            </form>
            </div>
            </div>
        </div>
        
    </div>
    @endforeach


    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="background-image: linear-gradient(315deg, #f7b42c 0%, #fc575e 74%);">
                <div class="modal-header text-white">
                    <h4 class="modal-title" id="exampleModalLabel">Tambah Data</h4>
                    <button type="button" class="close" style="color: white;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body text-white">
                <form action="{{ route('datamaster.storeclient') }}" method="POST" id="editform">
                @csrf
                <div class="form-group">
                    <label>ID Client</label>
                    <input type="text" name="id_client" value ="{{ 'CS-'.$kd}}" class="form-control" readonly>
                </div>
                <div class="form-group">
                    <label>Nomor Client</label>
                    <input type="text" name="nomor_client" value ="{{ 'CL-'.$kd}}" class="form-control" readonly>
                </div>
                <div class="form-group">
                    <label>Nama Client</label>
                    <input type="text" name="nama_client" class="form-control" placeholder="Nama Client" required>
                </div>
                <div class="form-group">
                    <label>Alamat</label>
                    <textarea type="textarea" name="alamat" class="form-control" rows="3" placeholder="Alamat" required></textarea>
                </div>
                <div class="form-group">
                    <label>Telepon</label>
                    <input type="number" name="telepon" class="form-control" placeholder="Telepon" required= "tidak boleh kosong">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" name="email" class="form-control" placeholder="Email" required= "tidak boleh kosong">
                </div>
                <div class="form-group">
                    <label>Jabatan</label>
                    <input type="text" name="jabatan" class="form-control" placeholder="Jabatan" required>
                </div>
                <div class="form-group">
                    <label>Perusahaan</label>
                    <input type="text" name="perusahaan" class="form-control" placeholder="Perusahaan" required>
                </div>
                <div class="form-group">
                    <label>Penanggung Jawab 1</label>
                    <input type="text" name="penanggung_jawab1" class="form-control" placeholder="Penanggung Jawab 1" required= "tidak boleh kosong">
                </div>
                <div class="form-group">
                    <label>Penanggung Jawab Jabatan 1</label>
                    <input type="text" name="penanggung_jawab_jabatan1" class="form-control" placeholder="Penanggung Jawab jabatan 1" required= "tidak boleh kosong">
                </div>
                <div class="form-group">
                    <label>Penanggung Jawab 2</label>
                    <input type="text" name="penanggung_jawab2" class="form-control" placeholder="Penanggung Jawab 2" required= "tidak boleh kosong">
                </div>
                <div class="form-group">
                    <label>Penanggung Jawab Jabatan 2</label>
                    <input type="text" name="penanggung_jawab_jabatan2" class="form-control" placeholder="Penanggung Jawab jabatan 2" required= "tidak boleh kosong">
                </div>
                <div class="form-group">
                    <label>Penanggung Jawab 3</label>
                    <input type="text" name="penanggung_jawab3" class="form-control" placeholder="Penanggung Jawab 3" required= "tidak boleh kosong">
                </div>
                <div class="form-group">
                    <label>Penanggung Jawab Jabatan 3</label>
                    <input type="text" name="penanggung_jawab_jabatan3" class="form-control" placeholder="Penanggung Jawab jabatan 3" required= "tidak boleh kosong">
                </div>
                
                <br />
                <div class="modal-footer">
                <center><button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button></center>
                <center><button type="submit" class="btn btn-primary">Simpan</button></center>
                </div>
            </form>
            </div>
            </div>
        </div>
        
    </div>

@endsection