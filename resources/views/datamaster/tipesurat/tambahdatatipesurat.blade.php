@extends('layouts.home.app')
@section('content')

<div class="container">
	<div class="row justify-content-center">
		<div class="col">
			<div class="container">
			<br>
			<h2><center>Tambah Data Tipe Surat</center></h2>
			<br />
			
			<br />
			@include('layouts.messages')
			<br />
			<form action="{{ route('datamaster.storetipesurat') }}" method="POST">
				@csrf
				<div class="form-group">
					<label>Id Tipe Surat</label>
					<input type="text" name="id_tipe_surat" class="form-control" placeholder="Id Tipe Surat">
				</div>
				<div class="form-group">
					<label>Nama Tipe Surat</label>
					<input type="text" name="nama_tipe_surat" class="form-control" placeholder="Nama Tipe Surat">
				</div>
				<div class="form-group">
					<label>Template Surat</label>
					<input type="text" name="template_surat" class="form-control" placeholder="Template Surat">
				</div>
				
				<br />
				<center>
					<button type="submit" class="btn btn-primary">Simpan</button>
					<a class="btn btn-danger" href="/datamaster/tipesurat" role="button">Cancel</a>
				</center>
			</form>
			</div>
			<br>
		</div>
	</div>
</div>


@endsection