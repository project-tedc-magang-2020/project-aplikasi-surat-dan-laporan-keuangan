@extends('layouts.home.app')
@section('content')

<div class="container">
	<div class="row justify-content-center">
		<div class="col">
			<div class="container">
			<br>
			<h2><center>Edit Data Tipe Surat</center></h2>
			<br />
			
			<br />
			@include('layouts.messages')
			<br />
			@foreach ($tipesurats as $ts)
			<form action="{{ url('/datamaster/tipesurat/update',  $ts->id_tipe_surat) }}" method="POST">
				{!! csrf_field() !!}
				<div class="form-group">
					<label>Id Tipe Surat</label>
					<input type="text" name="id_tipe_surat" class="form-control" required="required" value="{{ $ts->id_tipe_surat }}" disabled="disabled">
				</div>
				<div class="form-group">
					<label>Nama Tipe Surat</label>
					<input type="text" name="nama_tipe_surat" class="form-control" required="required" value="{{ $ts->nama_tipe_surat }}">
				</div>
				<div class="form-group">
					<label>Template Surat</label>
					<input type="text" name="template_surat" class="form-control" required="required" value="{{ $ts->template_surat }}">
				</div>

				<center><button type="submit" class="btn btn-primary">Simpan</button></center>
			</form>
			@endforeach
			</div>
			<br>
		</div>
	</div>
</div>

<script>
        $('#datepicker').datepicker({
        	format: "dd-mm-yyyy",
            uiLibrary: 'bootstrap4'
        });
    </script>

@endsection