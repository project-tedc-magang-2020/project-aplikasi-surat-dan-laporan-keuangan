@extends('layouts.home.app')
@section('content')


<!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="container">
                    <h2 class="h3 mb-2 text-gray-800">Invoice</h2>
                    <br />
                @include('layouts.messages')
                <br />
                    </div>
                    <br/>

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4" style="background-image: linear-gradient(315deg, #f7b42c 0%, #fc575e 74%); width: 100%;">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" style="background-color: white; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="vertical-align: middle; text-align: center; width: 10%;">No.</th>
                                            <th style="vertical-align: middle; text-align: center; width: 25%;">Nomor Surat</th>
                                            <th style="vertical-align: middle; text-align: center; width: 25%;">Sub Total</th>
                                            <th style="vertical-align: middle; text-align: center; width: 25%;">Total Harga</th>
                                            <th style="vertical-align: middle; text-align: center; width: 15%;">Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                    </tfoot>
                                    
                                    <tbody>
                                    <?php $no=1; ?>
                                    @foreach ($data as $row)
                                        <tr>
                                            <td style="vertical-align: middle; text-align: center;">{{$no++}}.</td>
                                            <td style="vertical-align: middle; text-align: center;">{{ $row->nomor_surat }}.</td>
                                            <td style="vertical-align: middle; text-align: center;">Rp. {{ number_format($row->total_harga,0,',','.') }}</td>
                                            <?php 
                                              $ppn = 11/100;
                                              $total_harga = $row->total_harga;
                                              $total = $total_harga * $ppn + $total_harga; 
                                            ?>
                                            <td style="vertical-align: middle; text-align: center;">Rp. {{ number_format($total,0,',','.') }}</td>
                                            <td style="text-align: center;">
                                                <div class="container mb-0">
                                                <button class="bi bi-eye-fill detailbtn btn btn-info col col-md-8 mb-2" data-toggle="modal" data-target="#myModal{{ $row->id_invoice }}" style="font-size: 1.3rem; color:white;" role="button"></button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->


            <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
            <!-- End of Main Content -->

    @foreach ($data as $row)
    <!-- Modal Detail -->
    <div id="myModal{{ $row->id_invoice }}" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
            <!-- konten modal-->
            <div class="modal-content" style="background-image: linear-gradient(315deg, #f7b42c 0%, #fc575e 74%);">
                <!-- heading modal -->
                <div class="modal-header text-white">
                <h4>Detail Data Invoice</h4>
                    <button type="button" style="color: white;" class="close" data-dismiss="modal">&times;</button>
                    <!-- <h4 class="modal-title">Ini adalah heading dari Modal</h4> -->
                </div>
                <!-- body modal -->
                <div class="modal-body">
                   
                <div class="row">
                    <div class="col-lg-12">
                    <table class="table table-bordered table-hover" style="background-color: white;">
                    <thead>
                        <tr>
                        <th width="30%">Nomor Surat</th>
                        <td width="70%">{{ $row->nomor_surat }}</td>
                        </tr>
                        <tr>
                        <th width="30%">Nama Bank</th>
                        <td width="70%">{{ $row->nama_bank }}</td>
                        </tr>
                        <tr>
                        <th width="30%">Cabang</th>
                        <td width="70%">{{ $row->cabang }}</td>
                        </tr>
                        <tr>
                        <th width="30%">Nomor Rekening</th>
                        <td width="70%">{{ $row->nomor_rekening }}</td>
                        </tr>
                        <tr>
                        <th width="30%">Atas Nama</th>
                        <td width="70%">{{ $row->atas_nama }}</td>
                        </tr>
                        
                        <tr>
                        <th width="30%">Sub Total</th>
                        <td width="70%">Rp. {{ number_format($row->total_harga,0,',','.') }}</td>
                        </tr>
                        <?php 
                          $ppn = $row->ppn/100;
                          $total_harga = $row->total_harga;
                          $biaya_ppn = $total_harga * $ppn;
                          $total = $total_harga * $ppn + $total_harga; 
                        ?>
                        <tr>
                        <th width="30%">Nilai PPN {{$row->ppn}}%</th>
                        <td width="70%">Rp. {{ number_format($biaya_ppn,0,',','.') }}</td>
                        </tr>
                        <tr>
                        <th width="30%">Total Harga</th>
                        <td width="70%">Rp. {{ number_format($total,0,',','.') }}</td>
                        </tr>
                    </thead>
                    </table>
                    </div>
                    </div>
                 </div>
                <!-- footer modal -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    @endforeach


@endsection