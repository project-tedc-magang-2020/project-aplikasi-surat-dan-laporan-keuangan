@extends('layouts.home.app')
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col">
			<div class="container">
				<a href="/surat/dashboardsurat">
					<button type="button" class="btn btn-secondary" style="float: right;">
  				<i class="bi bi-arrow-counterclockwise" width="20" height="20" fill="currentColor"></i>
              </button>
				</a>
			<br>
			<br>
			<div class="container">
			<h3><center>Buat Surat Invoice</center></h3>
			<br />
			@include('layouts.messages')
			<br />
			<div class="container">
			<div class="card shadow mb-4" style="background-image: linear-gradient(315deg, #f7b42c 0%, #fc575e 74%); width: 90%; margin: auto;">
            <div class="card-body" style="width: 100%;">
			<form action="{{ route('invoice.store') }}" method="POST">
				@csrf
				<!--<div class="form-group">
					<label>ID</label>
					<input type="text" name="id_client" class="form-control" placeholder="Id Client">
				</div>-->
				<input type="text" name="id_surat" value ="{{ 'SR-'.$kd}}" class="form-control" hidden>
				<input type="text" name="id_tipe_surat" value ="TS-00001" class="form-control" hidden>
				<div class="form-group">
                    <label>Client</label>
                    <select name="id_client" class="form-select" required>
	  					<option value="" hidden>Client</option>
	  					@foreach ($clients as $pe)
	 					<option value="{{ $pe->id_client }}">{{ $pe->nama_client }}</option>
	 					@endforeach
					</select>
                </div>
				<div class="form-group">
					<label>Karyawan</label>
					<select name="id_karyawan" class="form-select" required>
	  					<option value="" hidden>Karyawan</option>
	  					@foreach ($karyawans as $kr)
	 					<option value="{{ $kr->id_karyawan }}">{{ $kr->nama_karyawan }}</option>
	 					@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>Nomor Surat</label>
					<input type="text" name="nomor_surat" value ="{{ $nomorbaru }}" class="form-control" readonly>
				</div>
				<div class="form-group">
                    <label>Tanggal Surat</label>
                    <input id="datepicker" name="tgl_surat" placeholder="Tanggal Surat" />
                </div>
                <div class="form-group">
					<label>Perihal Surat</label>
					<input type="text" name="perihal_surat" class="form-control" placeholder="Perihal Surat" required= "tidak boleh kosong">
				</div>
                <div class="form-group">
					<label>Deskripsi Atas</label>
					<textarea type="text" name="deskripsi_atas" class="my-editor form-control" id="my-editor" placeholder="Deskripsi Atas" cols="30" rows="10" required= "tidak boleh kosong"></textarea>
				</div>
                <div class="form-group">
					<label>Deskripsi Bawah</label>
					<textarea type="text" name="deskripsi_bawah" class="my-editor1 form-control" id="my-editor1" cols="30" rows="10" required= "tidak boleh kosong"></textarea>
				</div>

				<div class="card shadow mb-4" style="background-color: orange; width: 100%;">
                <div class="card-body">
                <center>
				<div class="row text-center control-group afteraddmore">
				  <div class="col-xl-5">
				    <!-- Name input -->
				    <div class="form-outline">
				    	<label class="form-label" for="form8Example3">Item</label>
				      <input type="text" name="nama_item[]" value="{{old('nama_item')}}" class="form-control" />
				    </div>
				  </div>
				  <div class="col-xl-5">
				    <!-- Name input -->
				    <div class="form-outline">
				    	<label class="form-label" for="form8Example4">Harga</label>
				      <input type="number" name="harga_item[]" value="{{old('harga_item')}}" class="form-control" />
				    </div>
				  </div>
				  <div class="col">
				    <!-- Email input -->
				    <div class="col" style="margin: 0; position: absolute; top: 65%; -ms-transform: translateY(-35%); transform: translateY(-35%);">
				    	<button class="btn btn-success addmore" type="button">
              				<i class="bi bi-plus-circle-fill"></i> Add
            			</button>
				    </div>
				  </div>
				</div>
				</center>

			</div>
			</div>
				<div class="form-group">
					<label>Nilai PPN (%)</label>
					<input type="number" name="ppn" class="form-control">
				</div>
				<div class="form-group">
					<label>Nama Bank</label>
					<input type="text" name="nama_bank" class="form-control">
				</div>
				<div class="form-group">
					<label>Cabang</label>
					<input type="text" name="cabang" class="form-control">
				</div>
				<div class="form-group">
					<label>Nomor Rekening</label>
					<input type="text" name="nomor_rekening" class="form-control">
				</div>
				<div class="form-group">
					<label>Atas Nama</label>
					<input type="text" name="atas_nama" class="form-control">
				</div>

					<input type="number" name="total_harga" value="" hidden>
					
				<br />
				<center>
					<button type="submit" class="btn btn-primary">Simpan</button>
					<a class="btn btn-danger" href="/surat/dashboardsurat" role="button">Cancel</a>
				</center>
			</form>
			<br>
			</div>
			</div>
			</div>

			</div>
			</div>
		</div>
	</div>
</div>

<script>
    $('#datepicker').datepicker({
        format: "dd-mm-yyyy",
        uiLibrary: 'bootstrap4'
    });
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>

<script type="text/javascript">
	$('.addmore').on('click', function(){
		addmore();
	});
	function addmore(){
		var afteraddmore = '<center><div class="copy hide"><div class="row text-center control-group"><div class="col-xl-5"><!-- Name input --><div class="form-outline"><label class="form-label" for="form8Example3">Item</label><input type="text" name="nama_item[]" value="{{old('nama_item')}}" class="form-control" /></div></div><div class="col-xl-5"><!-- Name input --><div class="form-outline"><label class="form-label" for="form8Example4">Harga</label><input type="number" name="harga_item[]" value="{{old('harga_item')}}" class="form-control" /></div></div><div class="col-xl-2"><!-- Email input --><div class="col" style="margin: 0; position: absolute; top: 65%; -ms-transform: translateY(-35%); transform: translateY(-35%);"><button class="btn btn-danger remove" type="button"><i class="bi bi-dash-circle-fill"></i> Remove</button></div></div></div></div></center>';
		$('.afteraddmore').append(afteraddmore);
	};
	$('.remove').live('click', function(){
		$(this).parent().parent().parent().remove();
	});
</script>


@endsection

@push('scripts')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('my-editor');
    </script>
<script>
    CKEDITOR.replace('my-editor1');
</script>
@endpush