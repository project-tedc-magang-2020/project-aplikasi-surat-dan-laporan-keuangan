@extends('layouts.home.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-6 mb-4">

<br />

<center>
	<div class="container">
    <a class="no-print btn btn-sm btn-warning" style="float: right; margin-right: 65px; font-size: 20px;" onclick="CreatePDFfromHTML()" ><span class="fa fa-download"></span> &nbsp; Download</a>
    <br>
    <div id="halaman1">
		
		<br />
		<br />
        <p class="p1">Bandung, {{ tanggal_indonesia1($data->tgl_surat) }}</p>
		<br />
		<div class="p1">
		<table>
            <tr>
                <td style="width: 45%;">Nomor</td>
                <td style="width: 5%;">:</td>
                <td style="width: 50%;">{{ $data->nomor_surat }}</td>
            </tr>
			<tr>
                <td style="width: 45%;">Lampiran</td>
                <td style="width: 5%;">:</td>
                <td style="width: 50%;"> - </td>
            </tr>
		</table>
		</div>
		<br />
		<div class="p1">
			<table>
				<tr>
					<td style="width: 40%;">Kepada Yth,</td>
				</tr>
				<tr>
					<td style="width: 40%;"><b>{{ $data->nama_client }}</b></td>
				</tr>
				<tr>
					<td style="width: 40%;"><b>{{ $data->alamat }}</b></td>
				</tr>
			</table>
		</div>
		<br />
		<div class="p1">
			<table>
            <tr>
                <td style="width: 45%;">Perihal</td>
                <td style="width: 5%;">:</td>
                <td style="width: 50%;">{{ $data->perihal_surat }}</td>
            </tr>
			</table>
		</div>
		<br />
		<p class="p2">Dengan hormat,</p>
		<p class="p2">{{ strip_tags($data->deskripsi_atas) }}</p>
        <table class="table table-bordered">
        <?php $no=1; ?>
  <thead>
    <tr style="text-align: center; font-family: Arial; font-size: 14pt;">
      <th scope="col" width="10%">No</th>
      <th scope="col" width="60%">Item</th>
      <th scope="col" width="30%">Jumlah</th>
    </tr>
  </thead>
  <tbody style="font-family: Arial; font-size: 12pt;">
    @foreach ($datas as $it)
    <tr>
      <th scope="row" style="text-align: center;">{{$no++}}.</th>
      <td>{{ $it->nama_item }} </td>
      <td>Rp. {{ number_format($it->harga_item,0,',','.') }}</td>
    </tr>
    @endforeach
    <tr>
      @foreach ($datat as $is)
      <th scope="row" colspan="2" style="text-align:right;">Sub Total &nbsp;</th>
      <td >Rp. {{ number_format($is->total_harga,0,',','.') }}</td>
    </tr>
    <?php 
      $ppn = $is->ppn/100;
      $total_harga = $is->total_harga;
      $biaya_ppn = $total_harga * $ppn;
      $total = $total_harga * $ppn + $total_harga; 
    ?>
    <tr>
      <th scope="row" colspan="2" style="text-align:right;">PPN {{$is->ppn}}% &nbsp;</th>
      <td >Rp. {{ number_format($biaya_ppn,0,',','.') }}</td>
    </tr>
  
    <tr>
      <th scope="row" colspan="2" style="text-align:right;">Total + PPN &nbsp;</th>
      <td >Rp. {{ number_format($total,0,',','.') }}</td>
    </tr>
      
    <tr>
      <th scope="row" colspan="3">&nbsp; Terbilang : {{ penyebut($total) }} Rupiah</th>
    </tr>
    
  </tbody>
  </table>
	<p class="p2">Mohon kiranya pembayaran tersebut dapat dikirim/ditransfer melalui rekening :</p>
	<div class="container">
	<div class="p2">
		<table>
            <tr>
                <td style="width: 45%;">Nama Bank</td>
                <td style="width: 5%;">:</td>
                <td style="width: 50%;">{{ $is->nama_bank }}</td>
            </tr>
			<tr>
                <td style="width: 45%;">Cabang</td>
                <td style="width: 5%;">:</td>
                <td style="width: 50%;">{{ $is->cabang }}</td>
            </tr>
			<tr>
                <td style="width: 45%;">Nomor Rekening</td>
                <td style="width: 5%;">:</td>
                <td style="width: 50%;">{{ $is->nomor_rekening }}</td>
            </tr>
			<tr>
                <td style="width: 45%;">Atas Nama</td>
                <td style="width: 5%;">:</td>
                <td style="width: 50%;">{{ $is->atas_nama }}</td>
            </tr>
		</table>
	</div>
	</div>
  @endforeach
    <br />
    <p class="p2">{{ strip_tags($data->deskripsi_bawah) }}</p>
    <br />

    <div class="p1"><b>Hormat kami,</b></div><br><br><br><br>
    <div class="p1"><b><u>{{ $data->nama_karyawan }}</u></b></div>
    @foreach ($datag as $jb)
    <div class="p1">{{ $jb->nama_jabatan }}</div>
    @endforeach
    <br><br><br>
    </div>
	</div>
</center>
    <br>

    </div>
</div>
</div>

<textarea id="printing-css" style="display:none;">html,body,halaman1{width: 1080px;height: 1430px;position: center;padding-top: 0px;padding-left: 0px;padding-right: 0px;padding-bottom: 0px;background-image: url("/images/b.jpg");background-size: 1097px 1570px;}.no-print{display:none}</textarea>
<iframe id="printing-frame" name="print_frame" src="#" style="display:none;"></iframe>

<script type="text/javascript">
function printDiv(elementId) {
 var a = document.getElementById('printing-css').value;
 var b = document.getElementById(elementId).innerHTML;
 window.frames["print_frame"].document.title = document.title;
 window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
 window.frames["print_frame"].window.focus();
 window.frames["print_frame"].window.print();
}
</script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>

<script type="text/javascript">
  //Create PDf from HTML...
function CreatePDFfromHTML() {
    var HTML_Width = $("#halaman1").width();
    var HTML_Height = $("#halaman1").height();
    var top_left_margin = 15;
    var PDF_Width = HTML_Width + (top_left_margin * 2);
    var PDF_Height = (PDF_Width * 1.5) + (top_left_margin * 2);
    var canvas_image_width = HTML_Width;
    var canvas_image_height = HTML_Height;

    var totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;

    html2canvas($("#halaman1")[0]).then(function (canvas) {
        var imgData = canvas.toDataURL("image/jpeg", 1.0);
        var pdf = new jsPDF('p', 'pt', [PDF_Width, PDF_Height]);
        pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, canvas_image_width, canvas_image_height);
        for (var i = 1; i <= totalPDFPages; i++) { 
            pdf.addPage(PDF_Width, PDF_Height);
            pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
        }
        pdf.save("suratinvoices.pdf");
    });
}
</script>
    
@endsection