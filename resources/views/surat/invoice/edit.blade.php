@extends('layouts.home.app')
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col">
			<div class="container">
				<a href="/surat/dashboardsurat">
					<button type="button" class="btn btn-secondary" style="float: right;">
  				<i class="bi bi-arrow-counterclockwise" width="20" height="20" fill="currentColor"></i>
              </button>
				</a>
			<br>
			<br>
			<div class="container">
			<h3><center>Edit Surat Invoice</center></h3>
			<br />
			@include('layouts.messages')
			<br />
			<div class="container">
			<div class="card shadow mb-4" style="background-image: linear-gradient(315deg, #f7b42c 0%, #fc575e 74%); width: 90%; margin: auto;">
            <div class="card-body" style="width: 100%;">
            @foreach ($data as $row)
			<form action="{{ url('/surat/datasurat/updateinvoice',  $row->id_surat) }}" method="POST" id="editform">
                {!! csrf_field() !!}
				<!--<div class="form-group">
					<label>ID</label>
					<input type="text" name="id_client" class="form-control" placeholder="Id Client">
				</div>-->
				<input type="text" name="id_surat" value ="{{ $row->id_surat }}" class="form-control" hidden>
				<input type="text" name="id_tipe_surat" value ="{{ $row->id_tipe_surat }}" class="form-control" hidden>
				<div class="form-group">
                    <label>Client</label>
                    <select name="id_client" class="form-select" required>
                        @foreach ($clients as $pe)
                            <option value="{{ $pe->id_client }}" {{ old('id_client', $row->id_client) == $pe->id_client ? 'selected' : null}}>{{ $pe->nama_client }}</option>
                        @endforeach
                    </select>
                </div>
				<div class="form-group">
                    <label>Karyawan</label>
                    <select name="id_karyawan" class="form-select" required>
                        @foreach ($karyawans as $kr)
                            <option value="{{ $kr->id_karyawan }}" {{ old('id_karyawan', $row->id_karyawan) == $kr->id_karyawan ? 'selected' : null}}>{{ $kr->nama_karyawan }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Nomor Surat</label>
                    <input type="text" name="nomor_surat" class="form-control" required="required" value="{{ $row->nomor_surat }}" readonly></input>
                </div>
                <div class="form-group">
                    <label>Tanggal Surat</label>
                    <input class="datepicker3"  name="tgl_surat" required="required" value="{{ $row->tgl_surat }}">
                </div>
                <div class="form-group">
                    <label>Perihal Surat</label>
                    <input type="text" name="perihal_surat" class="form-control" required="required" value="{{ $row->perihal_surat }}"></input>
                </div>
                <div class="form-group">
                    <label>Deskripsi Atas</label>
                    <textarea type="text" name="deskripsi_atas" class="form-control" id="my-editor2{{ $row->id_surat }}" required="required" cols="30" rows="10">{{ strip_tags($row->deskripsi_atas) }}</textarea>
                </div>
                <div class="form-group">
                    <label>Deskripsi Bawah</label>
                    <textarea type="text" name="deskripsi_bawah" class="form-control" id="my-editor3{{ $row->id_surat }}" required="required" cols="30" rows="10">{{ strip_tags($row->deskripsi_bawah) }}</textarea>
                </div>

				<div class="card shadow mb-4" style="background-color: orange; width: 100%;">
                <div class="card-body">
                <center>
				<div class="afteraddmore">
					@foreach ($datas as $it)
					<div class="row text-center control-group">
				  <div class="col-xl-5">
				    <!-- Name input -->
				    <div class="form-outline">
				    	<label class="form-label" for="form8Example3">Item</label>
				      <input type="text" name="nama_item[]" value="{{ $it->nama_item }}" class="form-control" />
				    </div>
				  </div>
				  <div class="col-xl-5">
				    <!-- Name input -->
				    <div class="form-outline">
				    	<label class="form-label" for="form8Example4">Harga</label>
				      <input type="number" name="harga_item[]" value="{{ $it->harga_item }}" class="form-control" />
				    </div>
				  </div>
				  <div class="col-xl-2">
				  	<!-- Email input -->
				  	<div class="col" style="margin: 0; position: absolute; top: 65%; -ms-transform: translateY(-35%); transform: translateY(-35%);">
				  		<button class="btn btn-danger remove" type="button"><i class="bi bi-dash-circle-fill"></i> Remove</button>
				  	</div>
				  </div>
				  </div>
				  @endforeach
				</div>
				
				</center>
				<br><br>
				<div class="col" style="text-align: center;"> 
				    <!-- Email input -->
				    <div class="col" style="margin: 0; position: absolute; top: 65%; -ms-transform: translateY(-35%); transform: translateY(-35%);">
				    	<button class="btn btn-success addmore" type="button">
              				<i class="bi bi-plus-circle-fill"></i> Add
            			</button>
				    </div>
				  </div>
				 <br>

			</div>
			</div>

			@foreach ($datat as $is)
			<div class="form-group">
					<label>Nilai PPN (%)</label>
					<input type="number" name="ppn" value="{{ $is->ppn }}" class="form-control">
				</div>
			<div class="form-group">
					<label>Nama Bank</label>
					<input type="text" name="nama_bank" value="{{ $is->nama_bank }}" class="form-control">
				</div>
				<div class="form-group">
					<label>Cabang</label>
					<input type="text" name="cabang" value="{{ $is->cabang }}" class="form-control">
				</div>
				<div class="form-group">
					<label>Nomor Rekening</label>
					<input type="text" name="nomor_rekening" value="{{ $is->nomor_rekening }}" class="form-control">
				</div>
				<div class="form-group">
					<label>Atas Nama</label>
					<input type="text" name="atas_nama" value="{{ $is->atas_nama }}" class="form-control">
				</div>
			@endforeach

					<input type="number" name="total_harga" value="" hidden>
				<br />
				<center>
					<button type="submit" class="btn btn-primary">Simpan</button>
					<a class="btn btn-danger" href="/surat/dashboardsurat" role="button">Cancel</a>
				</center>
			</form>
			@endforeach
			<br>
			</div>
			</div>
			</div>

			</div>
			</div>
		</div>
	</div>
</div>

<script>
    $('.datepicker3').datepicker({
        format: "dd-mm-yyyy",
        uiLibrary: 'bootstrap4'
    });
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>

<script type="text/javascript">
	$('.addmore').on('click', function(){
		addmore();
	});
	function addmore(){
		var afteraddmore = '<center><div class="copy hide"><div class="row text-center control-group"><div class="col-xl-5"><!-- Name input --><div class="form-outline"><label class="form-label" for="form8Example3">Item</label><input type="text" name="nama_item[]" value="{{old('nama_item')}}" class="form-control" /></div></div><div class="col-xl-5"><!-- Name input --><div class="form-outline"><label class="form-label" for="form8Example4">Harga</label><input type="number" name="harga_item[]" value="{{old('harga_item')}}" class="form-control" /></div></div><div class="col-xl-2"><!-- Email input --><div class="col" style="margin: 0; position: absolute; top: 65%; -ms-transform: translateY(-35%); transform: translateY(-35%);"><button class="btn btn-danger remove" type="button"><i class="bi bi-dash-circle-fill"></i> Remove</button></div></div></div></div></center>';
		$('.afteraddmore').append(afteraddmore);
	};
	$('.remove').live('click', function(){
		$(this).parent().parent().parent().remove();
	});
</script>


@endsection

@push('scripts')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('my-editor');
    </script>
<script>
    CKEDITOR.replace('my-editor1');
</script>
@endpush