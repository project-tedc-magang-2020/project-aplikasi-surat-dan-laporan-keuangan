@extends('layouts.home.app')
@section('content')

<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="container">
			<br>
			<h2><center>Tambah Data Project</center></h2>
			<a href="/datamaster/client/" class="btn btn-danger" role="button">Kembali</a> <br>
			<br />

			@include('layouts.messages')
			<br />
			<form action="{{ route('surat.storeproject') }}" method="POST">
				@csrf
				<fieldset>
					<legend>form tambah</legend>
                <div class="form-group">
					<label>Id Client</label>
					<select name="id_client" class="form-select" required>
	  					<option value="" hidden>Id Client</option>
	  					@foreach ($clients as $cli)
	 					<option value="{{ $cli->id_client }}">{{ $cli->id_client }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>Nomor Project</label>
					<input type="text" name="nomor_project" class="form-control"  value ="{{ 'PRJ-'.$kd}}" class="form-control" readonly>
				</div>
                <div class="form-group">
					<label>Nama Project</label>
					<input type="text" name="nama_project" class="form-control"  class="form-control" >
				</div>
                <div class="form-group">
					<label> Tanggal Mulai</label>
					<input type="date" name="tgl_mulai" class="form-control" placeholder="Tanggal Mulai" >
				</div>
                <div class="form-group">
					<label> Tanggal Selesai</label>
					<input type="date" name="tgl_selesai" class="form-control" placeholder="Tanggal Selesai">
				</div>
                <div class="form-group">
					<label> nilai project</label>
					<input type="number" name="nilai_project" class="form-control" placeholder="Nilai Project">
				</div>
				
				<br />
				<center><button type="submit" class="btn btn-primary">Simpan</button></center>
				</fieldset>
			</form>
			</div>
			<br>
		</div>
	</div>
</div>


@endsection