@extends('layouts.home.app')
@section('content')

<div class="container">
	<div class="row justify-content-center">
		<div class="col">
			<div class="container">
			<br>
			<h2><center>Edit Data Project</center></h2>
			<br />
			
			<br />
			@include('layouts.messages')
			<br />
			@foreach ($projects as $pr)
			<form action="{{ url('/surat/project/update',  $pr->id_project) }}" method="POST">
				{!! csrf_field() !!}
                <div class="form-group">
					<label>Id Client</label>
					<select name="id_client" class="form-select" required>
	  					<option value="" hidden>Id Client</option>
	  					@foreach ($clients as $cli)
						  <option value="{{ $cli->id_client }}" {{ old('id_client', $cli->id_client) == $cli->id_client ? 'selected' : null}}>{{ $cli->id_client }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>Nomor Project</label>
					<input type="text" name="nomor_project" class="form-control" required="required" value="{{ $pr->nomor_project }}">
				</div>
				<div class="form-group">
					<label>Nama Project</label>
					<input type="text" name="nama_project" class="form-control" required="required" value="{{ $pr->nama_project }}">
				</div>
                <div class="form-group">
					<label>Tanggal Mulai</label>
					<input type="date" name="tgl_mulai" class="form-control" required="required" value="{{ $pr->tgl_mulai }}">
				</div>
                <div class="form-group">
					<label>Tanggal Selesai</label>
					<input type="date" name="tgl_selesai" class="form-control" required="required" value="{{ $pr->tgl_selesai }}">
				</div>
                <div class="form-group">
					<label>Nilai Project</label>
					<input type="date" name="nilai_project" class="form-control" required="required" value="{{ $pr->nilai_project }}">
				</div>

				<center><button type="submit" class="btn btn-primary">Simpan</button></center>
			</form>
			@endforeach
			</div>
			<br>
		</div>
	</div>
</div>

<script>
        $('#datepicker').datepicker({
        	format: "dd-mm-yyyy",
            uiLibrary: 'bootstrap4'
        });
    </script>

@endsection