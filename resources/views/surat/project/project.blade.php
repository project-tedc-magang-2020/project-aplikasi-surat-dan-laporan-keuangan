@extends('layouts.home.app')
@section('content')


<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="container">
        <h2 class="h3 mb-2 text-gray-800">Daftar Project</h2>
        <br />
        @include('layouts.messages')
        <br />
        <a data-toggle="modal" data-target="#addModal">
            <button class="bi bi-plus-circle-fill btn" style="background-image: linear-gradient(315deg, #f7b42c 0%, #fc575e 74%); color:white;"> Tambah Data</button>
        </a>
    </div>
    <br/>

    <!-- DataTales Example -->
    <div class="card shadow mb-4" style="background-image: linear-gradient(315deg, #f7b42c 0%, #fc575e 74%); width: 100%;">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" style="background-color: white; width: 100%;">
                    <thead>
                        <tr>
                            <th style="vertical-align: middle; text-align: center;">Nomor Project</th>
                            <th style="vertical-align: middle; text-align: center;">Nama Client</th>
                            <th style="vertical-align: middle; text-align: center;">Nama Project</th>
                            <th style="vertical-align: middle; text-align: center;">Nilai Project</th>
                            
                            <th style="vertical-align: middle; text-align: center;"><center>Action</center></th>
                        </tr>
                    </thead>
                    <tfoot>
                    </tfoot>
                    
                    <tbody>
                        @foreach ($data as $pr)
                        <tr>
                            <td style="vertical-align: middle; text-align: center;">{{ $pr->nomor_project}}</td>
                            <td style="vertical-align: middle; text-align: center;">{{ $pr->nama_client}}</td>
                            <td style="vertical-align: middle; text-align: center;">{{ $pr->nama_project}}</td>
                            <td style="vertical-align: middle; text-align: center;">Rp.{{ number_format($pr->nilai_project, 0, ',','.')}}</td>
                            <td style="text-align: center; vertical-align=:middle;">
                                <div class="container mb-0">
                                    <button data-toggle="modal" data-target="#editModal{{ $pr->id_project }}" class="bi bi-pencil-square editbtn btn btn-warning col-xl-3 col-md-4 mb-2" style="font-size: 1.3rem; color:white;" role="button"></button>
                                    <button data-toggle="modal" data-target="#my-modal{{ $pr->id_project }}" class="bi bi-trash-fill btn btn-danger col-xl-3 col-md-4 mb-2" style="font-size: 1.2rem; color:white;" role="button"></button>
                                    <button class="bi bi-eye-fill detailbtn btn btn-info col-xl-3 col-md-4 mb-2" data-toggle="modal" data-target="#myModal{{ $pr->id_project }}" style="font-size: 1.3rem; color:white;" role="button"></button>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->


<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
<!-- End of Main Content -->
@foreach ($projects as $pr)
<div id="my-modal{{ $pr->id_project }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content border-0">   
            <div class="modal-body p-0">
                <div class="card border-0 p-sm-3 p-2 justify-content-center">
                    <div class="card-header pb-0 bg-white border-0 "><div class="row"><div class="col ml-auto"><button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div> </div>
                    <p class="font-weight-bold mb-2"> Are you sure you wanna delete this ?</p><p class="text-muted "> These changes will be visible on your portal and the data will be deleted.</p>     </div>
                    <div class="card-body px-sm-4 mb-2 pt-1 pb-0"> 
                        <div class="row justify-content-end no-gutters"><div class="col-auto"><button type="button" class="btn btn-light text-muted" data-dismiss="modal">Cancel</button><a class="btn btn-danger px-4" href="/surat/project/{{ $pr->id_project }}" role="button">Delete</a></div><div class="col-auto"></div></div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>
@endforeach

@foreach ($projects as $pr)
<!-- Modal Detail -->
<div id="myModal{{ $pr->id_project }}" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- konten modal-->
        <div class="modal-content" style="background-image: linear-gradient(315deg, #f7b42c 0%, #fc575e 74%); color:white;">
            <!-- heading modal -->
            <div class="modal-header text-white">
                <h4>Detail Data Project</h4>
                <button type="button" class="close" style="color: white;" data-dismiss="modal">&times;</button>
                <!-- <h4 class="modal-title">Ini adalah heading dari Modal</h4> -->
            </div>
            <!-- body modal -->
            <div class="modal-body">
               
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-bordered table-hover" style="background-color: white;">
                            <thead>
                                <tr>
                                    <th>ID Client</th>
                                    <td>{{ $pr->id_client }}</td>
                                </tr>
                                <tr>
                                    <th>Nomor Project</th>
                                    <td>{{ $pr->nomor_project }}</td>
                                </tr>
                                <tr>
                                    <th>Nama Project</th>
                                    <td>{{ $pr->nama_project}}</td>
                                </tr>
                                <tr>
                                    <th>Tanggal Mulai</th>
                                    <td>{{ tanggal_indonesia1($pr->tgl_mulai) }}</td>
                                </tr>
                                <tr>
                                    <th>Tanggal Selesai<t/th>
                                        <td>{{ tanggal_indonesia1($pr->tgl_selesai) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Lokasi</th>
                                        <td>{{ $pr->lokasi}}</td>
                                    </tr>
                                    <tr>
                                        <th>Nilai Project</th>
                                        <td>{{ $pr->nilai_project}}</td>
                                    </tr>
                                    
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- footer modal -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    @endforeach

    @foreach ($projects as $pr)
    <div class="modal fade" id="editModal{{ $pr->id_project }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="background-image: linear-gradient(315deg, #f7b42c 0%, #fc575e 74%); color:white;">
                <div class="modal-header text-white">
                    <h4 class="modal-title" id="exampleModalLabel">Edit Data</h4>
                    <button type="button" class="close" style="color: white;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body text-white">
                    <form action="{{ url('/surat/project/update',  $pr->id_project) }}" method="POST" id="editform">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label>Nama Client</label>
                            <select name="id_client" class="form-select" required>
                                @foreach ($clients as $cli)
                                <option value="{{ $cli->id_client }}" {{ old('id_client', $cli->id_client) == $cli->id_client ? 'selected' : null}}>{{ $cli->id_client }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nomor Project</label>
                            <input type="text" name="nomor_project" class="form-control" required="required" value="{{ $pr->nomor_project }}">
                        </div>
                        <div class="form-group">
                            <label>Nama Project</label>
                            <input type="text" name="nama_project" class="form-control" required="required" value="{{ $pr->nama_project }}">
                        </div>
                        <div class="form-group">
                            <label>Tanggal Mulai</label>
                            <input name="tgl_mulai" class="form-control datepicker3" required="required" value="{{ $pr->tgl_mulai }}">
                        </div>
                        <div class="form-group">
                            <label>Tanggal Selesai</label>
                            <input name="tgl_selesai" class="form-control datepicker4" required="required" value="{{ $pr->tgl_selesai }}">
                        </div>
                        <div class="form-group">
                            <label>Lokasi</label>
                            <input type="text" name="lokasi" class="form-control" required="required" value="{{ $pr->lokasi }}">
                        </div>
                        <div class="form-group">
                            <label>Nilai Project</label>
                            <input type="number" name="nilai_project" class="form-control" required="required" value="{{ $pr->nilai_project }}">
                        </div>

                        
                        <br />
                        <div class="modal-footer">
                            <center><button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button></center>
                            <center><button type="submit" class="btn btn-primary">Simpan</button></center>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
    </div>
    @endforeach


    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="background-image: linear-gradient(315deg, #f7b42c 0%, #fc575e 74%); color:white;">
                <div class="modal-header text-white">
                    <h4 class="modal-title" id="exampleModalLabel">Tambah Data</h4>
                    <button type="button" class="close" style="color: white;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body text-white">
                    <form action="{{ route('surat.storeproject') }}" method="POST">
                        @csrf
                        <fieldset>
                            <div class="form-group">
                                <label>Id Project</label>
                                <input type="text" name="id_project" class="form-control"  value ="{{ 'PRJS-'.$kd}}" class="form-control" readonly>
                            </div>

                            <div class="form-group">
                                <label>Id Client</label>
                                <select name="id_client" class="form-select" required>
                                    <option value="" hidden>nama Client</option>
                                    @foreach ($clients as $cli)
                                    <option value="{{ $cli->id_client }}">{{ $cli->nama_client }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nomor Project</label>
                                <input type="text" name="nomor_project" class="form-control"  value ="{{ 'PRJ-'.$kd}}" class="form-control" readonly>
                            </div>
                            <div class="form-group">
                                <label>Nama Project</label>
                                <input type="text" name="nama_project" class="form-control"  class="form-control" placeholder="Nama Project">
                            </div>
                            <div class="form-group">
                                <label> Tanggal Mulai</label>
                                <input id="datepicker"  name="tgl_mulai" class="form-control" placeholder="Tanggal Mulai" >
                            </div>
                            <div class="form-group">
                                <label> Tanggal Selesai</label>
                                <input id="datepicker2" name="tgl_selesai" class="form-control" placeholder="Tanggal Selesai">
                            </div>
                            <div class="form-group">
                                <label>Lokasi</label>
                                <input type="text" name="lokasi" class="form-control"  class="form-control" placeholder="Lokasi" >
                            </div>
                            <div class="form-group">
                                <label> Nilai Project</label>
                                <input type="number" name="nilai_project" class="form-control" placeholder="Nilai Project">
                            </div>
                            <br />
                            <div class="modal-footer">
                                <center><button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button></center>
                                <center><button type="submit" class="btn btn-primary">Simpan</button></center>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
        </div>
        <script>
            $('#datepicker').datepicker({
            format: "dd-mm-yyyy",
            uiLibrary: 'bootstrap4'
        });
        </script>
         <script>
            $('#datepicker2').datepicker({
            format: "dd-mm-yyyy",
            uiLibrary: 'bootstrap4'
        });
        </script>
        <script>
            $('.datepicker3').datepicker({
            format: "dd-mm-yyyy",
            uiLibrary: 'bootstrap4'
        });
        </script>
         <script>
            $('.datepicker4').datepicker({
            format: "dd-mm-yyyy",
            uiLibrary: 'bootstrap4'
        });
        </script>

        @endsection