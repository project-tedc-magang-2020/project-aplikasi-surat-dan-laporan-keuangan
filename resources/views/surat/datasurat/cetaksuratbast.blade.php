@extends('layouts.home.app')
@section('content')

<div class="container">
  <div class="row">
    <div class="col-xl-12 col-md-6 mb-6">
     <a class="no-print btn btn-sm btn-warning" style="float: right; margin-right: 65px; font-size: 20px;" onclick="CreatePDFfromHTML()" ><span class="fa fa-download"></span> &nbsp; Download</a>
     </br> </br>
     <center>
      <?php
        $hariBahasaInggris = date('l', strtotime($data->tgl_surat));
        $hariBahasaIndonesia = hariIndo($hariBahasaInggris);
      ?>
       <div class="container" style="background-color: white;">
        <div class="col-xl-12">
         <div id=halaman1>
          <h4 class="upper" id="judulatas" style="text-align: center;" ><b>BERITA ACARA UJI TERIMA</b></h4>
          <h4 style="text-align: center;"><b>LIVE APPLICATION OPERATION SUPPORT</b></h4>
          <h4 class="upper" style="text-align: center;"><b>{{ strtoupper($data->nama_project) }}</b></h4>
          <div style="border-top: 2px ridge black; margin-top: 1em; padding-top: 1em; width: 100%;"> </div>
          </br></br>
          <p class="p1">Pada hari {{$hariBahasaIndonesia}}, {{ tanggal_indonesia1($data->tgl_surat) }}, bertempat di {{ $data->lokasi }} antara pihak-pihak :</p>
          
          <div class="p1">
            <table class="table table-borderless table table-sm">
              <tr>
                <td style="width: 2%">I.</td>
                <td style="width: 15%;">&nbsp;Nama</td>
                <td style="width: 5%;">:</td>
                <td style="width: 78%;"><span>{{ $data->nama_client }}</span></td>
              </tr>
              <tr>
                <td></td>
                <td style="width: 7%;">&nbsp;Jabatan</td>
                <td style="width: 5%;">:</td>
                <td style="width: 5%;">{{ $data->jabatan }}</td>
              </tr>
              <tr>
                <td></td>
                <td style="width: 5%;">&nbsp;Alamat</td>
                <td style="width: 5%;">:</td>
                <td style="width: 5%;">{{ $data->alamat }}</td>
              </tr>
            </table>
          </div>
          <p class="p1">Dalam hal ini bertindak untuk dan atas nama {{ $data->perusahaan }} yang selanjutnya disebut sebagai <b>PIHAK PERTAMA.</b></p>
     

          <div class="p1">
            <table class="table table-borderless table table-sm">
              <tr>
                <td style="width: 2%">II.</td>
                <td style="width: 15%;">&nbsp;Nama</td>
                <td style="width: 5%;">:</td>
                <td style="width: 78%;">{{ $data->nama_karyawan }}</td>
              </tr>
              <tr>
                <td></td>
                <td style="width: 7%;">&nbsp;Jabatan</td>
                <td style="width: 5%;">:</td>
                @foreach ($datag as $dt)
                <td style="width: 5%;">{{ $dt->nama_jabatan }}</td>
                @endforeach
              </tr>
              <tr>
                <td></td>
                <td style="width: 5%;">&nbsp;Alamat</td>
                <td style="width: 5%;">:</td>
                <td style="width: 5%;">Jl. Cibogo Atas No.144 Sukawarna, Bandung</td>
              </tr>
            </table>
          </div>
          <p class="p1">Dalam hal ini bertindak untuk dan atas nama PT. Crop Inspirasi Digital, yang selanjutnya disebut sebagai <b>PIHAK KEDUA.</b></p>
          <br>

          <p class="p1">Bahwa kedua belah pihak menyatakan sebagai berikut:</p>

          <ol class="p2">
            <li>Pada tanggal {{ $data->tgl_surat}} (<i>User Acceptance Testing</i>) {{ $data->nama_project}} oleh <b>PIHAK PERTAMA</b>, dengan di dampingi <b>PIHAK KEDUA.</b></li>
            <li>Dokumen UAT adalah dokumen hasil pengujian yang sudah dilakukan oleh kedua belah pihak dan dinyatakan menghasilkan revisi pada dokumen yang telah diselesaikan oleh pihak kedua.</li>
            <li>Telah dilakukan <i>Developmen To Production (D2P)</i> Tanggal {{ $data->tgl_surat }} sehingga<i> {{ $data->nama_project }} sudah dapat dimulai.</i></li>
          </ol>
          <br>
        

          <p class="p2">Demikian berita acara ini disepakati dan ditandatangani oleh kedua belah pihak untuk digunakan sebagaimana mestinya.</p>

          <br />
          <br />

          <center>
            <br><br><br>
          <table>
            <tbody>
              <tr class="p2">     
                <td>
                 <div style="width: 180%; text-align: left; float: right;">PIHAK PERTAMA </div><br><br><br><br>
                 <div style="width: 150%; text-align: left; float: right;"><b><u>{{ $data->nama_client }}</u></b></div>
                 <div style="width: 160%; text-align: left; float: right;">{{ $data->jabatan }} </div>
               </td>     
               <td></td>     
               <td valign="top"><div align="center">
                <div style="width: 170%; text-align: right; float: left;">PIHAK KEDUA </div><br><br><br><br>
                <div style="width: 150%; text-align: right; float: left;"><b><u>{{ $data->nama_karyawan }}</u></b></div>
                @foreach ($datag as $dt)
                <div style="width: 160%; text-align: right; float: left;">{{ $dt->nama_jabatan }} </div>@endforeach
              </td>  
            </tr>
          </tbody>
        </table>
      </center>
      <br><br><br>
      </div>

    </div>
  </div>

</center>

</div>
</div>
</div>


<textarea id="printing-css" style="display:none;">html,body,halaman{width: 1080px;height: 1430px;position: center;padding-top: 0px;padding-left: 0px;padding-right: 0px;padding-bottom: 0px;background-image: url("/images/b.jpg");background-size: 1097px 1570px;}.no-print{display:none}</textarea>
<iframe id="printing-frame" name="print_frame" src="#" style="display:none;"></iframe>  


<script type="text/javascript">
  function printDiv(elementId) {
   var a = document.getElementById('printing-css').value;
   var b = document.getElementById(elementId).innerHTML;
   window.frames["print_frame"].document.title = document.title;
   window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
   window.frames["print_frame"].window.focus();
   window.frames["print_frame"].window.print();
 }
</script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>

<script type="text/javascript">
  //Create PDf from HTML...
function CreatePDFfromHTML() {
    var HTML_Width = $("#halaman1").width();
    var HTML_Height = $("#halaman1").height();
    var top_left_margin = 15;
    var PDF_Width = HTML_Width + (top_left_margin * 2);
    var PDF_Height = (PDF_Width * 1.5) + (top_left_margin * 2);
    var canvas_image_width = HTML_Width;
    var canvas_image_height = HTML_Height;

    var totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;

    html2canvas($("#halaman1")[0]).then(function (canvas) {
        var imgData = canvas.toDataURL("image/jpeg", 1.0);
        var pdf = new jsPDF('p', 'pt', [PDF_Width, PDF_Height]);
        pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, canvas_image_width, canvas_image_height);
        for (var i = 1; i <= totalPDFPages; i++) { 
            pdf.addPage(PDF_Width, PDF_Height);
            pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
        }
        pdf.save("suratbast.pdf");
    });
}
</script>

@endsection