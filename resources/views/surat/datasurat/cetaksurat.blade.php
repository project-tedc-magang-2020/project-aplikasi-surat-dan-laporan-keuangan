@extends('layouts.home.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-xl-12 col-md-6 mb-4">

<br />
<center>
	<div class="container">
    <a class="no-print btn btn-sm btn-warning" style="float: right; margin-right: 65px; font-size: 20px;" href="javascript:printDiv('halaman1');" ><span class="fas fa-fw fa-print"></span>Print</a>
    <br>
    <div id="halaman1">
		
		<br />
		<br />

        <p class="p1">Bandung,  {{ tanggal_indonesia1($data->tgl_surat) }}</p>
		<br />
		<div class="p1">
		<table>
            <tr>
                <td style="width: 45%;">Nomor</td>
                <td style="width: 5%;">:</td>
                <td style="width: 55%;">{{ $data->nomor_surat }}</td>
            </tr>
			<tr>
                <td style="width: 45%;">Lampiran</td>
                <td style="width: 5%;">:</td>
                <td style="width: 55%;"> - </td>
            </tr>
		</table>
		</div>
		<br />
		<div class="p1">
			<table>
				<tr>
					<td style="width: 40%;">Kepada Yth,</td>
				</tr>
				<tr>
					<td style="width: 40%;"><b>{{ $data->nama_client }}</b></td>
				</tr>
				<tr>
					<td style="width: 40%;"><b>{{ $data->alamat }}</b></td>
				</tr>
			</table>
		</div>
		<br />
		<div class="p1">
			<table>
            <tr>
                <td style="width: 45%;">Perihal</td>
                <td style="width: 5%;">:</td>
                <td style="width: 55%;">{{ $data->perihal_surat }}</td>
            </tr>
			</table>
		</div>
		<br />
		<p class="p2">Dengan hormat,</p>
		<p class="p2">{{ strip_tags($data->deskripsi_atas) }}</p>
        <table class="table table-bordered">
        <?php $no=1; ?>
  <thead>
    <tr style="text-align: center; font-family: Arial; font-size: 14pt;">
      <th scope="col" width="10%">No</th>
      <th scope="col" width="65%">Item</th>
      <th scope="col" width="25%">Jumlah</th>
    </tr>
  </thead>
  <tbody style="font-family: Arial; font-size: 12pt;">
    @foreach ($datas as $it)
    <tr>
      <th scope="row" style="text-align: center;">{{$no++}}.</th>
      <td>{{ $it->nama_item }} </td>
      <td>Rp. {{ number_format($it->harga_item,0,',','.') }}</td>
    </tr>
    @endforeach
    <tr>
      @foreach ($datat as $is)
      <th scope="row" colspan="2" style="text-align:right;">Sub Total &nbsp;</th>
      <td >Rp. {{ number_format($is->total_harga,0,',','.') }}</td>
    </tr>
	<tr>
      <th scope="row" colspan="2" style="text-align:right;">Total &nbsp;</th>
      <td >Rp. {{ number_format($is->total_harga,0,',','.') }}</td>
    </tr>
      
	<tr>
      <th scope="row" colspan="3">&nbsp; Terbilang : {{ penyebut($is->total_harga) }} Rupiah</th>
    </tr>
    @endforeach
  </tbody>
  </table>
	<p class="p2">Mohon kiranya pembayaran tersebut dapat dikirim/ditransfer melalui rekening :</p>
	<div class="container">
	<div class="p2">
		<table>
            <tr>
                <td style="width: 45%;">Nama Bank</td>
                <td style="width: 5%;">:</td>
                <td style="width: 50%;">Bank Mandiri</td>
            </tr>
			<tr>
                <td style="width: 45%;">Cabang</td>
                <td style="width: 5%;">:</td>
                <td style="width: 50%;">Gatot Subroto Bandung</td>
            </tr>
			<tr>
                <td style="width: 45%;">Nomor Rekening</td>
                <td style="width: 5%;">:</td>
                <td style="width: 50%;">131-00-1716997-2</td>
            </tr>
			<tr>
                <td style="width: 45%;">Atas Nama</td>
                <td style="width: 5%;">:</td>
                <td style="width: 50%;">PT. Crop Digital Inspirasi</td>
            </tr>
		</table>
	</div>
	</div>
    <br />
    <p class="p2">{{ strip_tags($data->deskripsi_bawah) }}</p>
    <br />

    <div class="p1"><b>Hormat kami,</b></div><br><br><br><br>
    <div class="p1"><b><u>{{ $data->nama_karyawan }}</u></b></div>
    @foreach ($datag as $jb)
    <div class="p1">{{ $jb->nama_jabatan }}</div>
    @endforeach
    <br><br>
    </div>
	</div>
</center>
    <br>

    </div>
</div>
</div>

<textarea id="printing-css" style="display:none;">html,body,halaman1{width:100%; height:100%; page-break-after:always; margin: -10; size: A4 landscape; position: relative; background-image: url("/images/b.jpg");background-size: 1090px 1570px;}.no-print{display:none}</textarea>
<iframe id="printing-frame" name="print_frame" src="#" style="display:none;"></iframe>

<script type="text/javascript">
function printDiv(elementId) {
 var a = document.getElementById('printing-css').value;
 var b = document.getElementById(elementId).innerHTML;
 window.frames["print_frame"].document.title = document.title;
 window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
 window.frames["print_frame"].window.focus();
 window.frames["print_frame"].window.print();
}
</script>


    
@endsection