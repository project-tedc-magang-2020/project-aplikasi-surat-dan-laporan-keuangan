@extends('layouts.home.app')
@section('content')



<!-- Begin Page Content -->
    <div class="container">
        <h2 class="h4 mb-0 text-gray-800">Buat Surat</h2>
    </div>
    <br>
    <br />
    <br />
  <center>
  <div class="row">

    @foreach ($tipesurats as $ts)
    <!-- Earnings (Monthly) Card Example -->
      <div class="col-xl-6 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-1" style="background-image: linear-gradient(315deg, #f7b42c 0%, #fc575e 74%); width: 24rem;">
          <div class="card-body" style="height: 20rem;">
            <div class="container">
              <center>
                <img src="{{URL::asset('images/invoices1.png')}}" width="220" height="220">
                <div class="card-body">
                  <a href="{{ URL::to('surat/createsurat/' . $ts->template_surat) }}" class="btn" role="button" style="background-color: #1f9eda; color: white;">{{ $ts->nama_tipe_surat }}</a>
                </div>
              </center>
            </div>
          </div>
        </div>
      </div>
      @endforeach

      </div>
      </center>
      <br><br><br>

            
@endsection