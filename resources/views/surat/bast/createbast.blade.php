@extends('layouts.home.app')
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col">
			<div class="container">
				<a href="/surat/dashboardsurat">
					<button type="button" class="btn btn-secondary" style="float: right;">
  				<i class="bi bi-arrow-counterclockwise" width="20" height="20" fill="currentColor"></i>
              </button>
				</a>
			<br>
			<br>
			<div class="container">
			<h3><center>Buat Surat BAST</center></h3>
			<br />
			@include('layouts.messages')
			<br />
			<div class="container">
			<div class="card shadow mb-4" style="background-image: linear-gradient(315deg, #f7b42c 0%, #fc575e 74%); width: 90%; margin: auto;">
            <div class="card-body" style="width: 100%;">
			<form action="{{ route('bast.storebast') }}" method="POST">
				@csrf
				<!--<div class="form-group">
					<label>ID</label>
					<input type="text" name="id_client" class="form-control" placeholder="Id Client">
				</div>-->
				<input type="text" name="id_surat" value ="{{ 'SR-'.$kd}}" class="form-control" hidden>
				<input type="text" name="id_tipe_surat" value ="TS-00002" class="form-control" hidden>
				<div class="form-group">
                    <label>Client</label>
                    <select name="id_client" class="form-select" required>
	  					<option value="" hidden>Client</option>
	  					@foreach ($clients as $pe)
	 					<option value="{{ $pe->id_client }}">{{ $pe->nama_client }}</option>
	 					@endforeach
					</select>
</div>
				<div class="form-group">
					<label>Karyawan</label>
					<select name="id_karyawan" class="form-select" required>
	  					<option value="" hidden>Karyawan</option>
	  					@foreach ($karyawans as $kr)
	 					<option value="{{ $kr->id_karyawan }}">{{ $kr->nama_karyawan }}</option>
	 					@endforeach
					</select>
				</div>
				
				<div class="form-group">
                    <label>Tanggal Surat</label>
                    <input id="datepicker" name="tgl_surat" placeholder="Tanggal Surat" />
                </div>

                <div class="form-group">
					<label>Deskripsi Atas</label>
					<textarea type="text" name="deskripsi_atas" class="my-editor form-control" id="my-editor" placeholder="Deskripsi Atas" cols="30" rows="10" required= "tidak boleh kosong"></textarea>
				</div>
				<div class="form-group">
					<label>Deskripsi Bawah</label>
					<textarea type="text" name="deskripsi_bawah" class="my-editor1 form-control" id="my-editor1" cols="30" rows="10" required= "tidak boleh kosong"></textarea>
				</div> 
				<div class="form-group">
					<label>Project</label>
					<select name="id_project" class="form-select" required>
	  					<option value="" hidden>Project</option>
	  					@foreach ($projects as $pr)
	 					<option value="{{ $pr->id_project }}">{{ $pr->nama_project }}</option>
	 					@endforeach
					</select>
				</div>
                <!-- <div class="form-group">
					<label>Deskripsi Bawah</label>
					<input type="text" name="deskripsi_bawah" class="form-control" placeholder="Deskripsi Bawah" required= "tidak boleh kosong">
				</div> -->
                
				<br />
				<center>
					<button type="submit" class="btn btn-primary">Simpan</button>
					<a class="btn btn-danger" href="/surat/dashboardsurat" role="button">Cancel</a>
				</center>
			</form>
			<br>
			</div>
			</div>
			</div>

			</div>
			</div>
		</div>
	</div>
</div>

<script>
    $('#datepicker').datepicker({
        format: "dd-mm-yyyy",
        uiLibrary: 'bootstrap4'
    });
</script>



@endsection

@push('scripts')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('my-editor');
    </script>
<script>
    CKEDITOR.replace('my-editor1');
</script>
@endpush