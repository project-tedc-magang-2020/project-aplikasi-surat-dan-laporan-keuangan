@extends('layouts.home.app')
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col">
			<div class="container">
				<a href="/surat/dashboardsurat">
					<button type="button" class="btn btn-secondary" style="float: right;">
  				<i class="bi bi-arrow-counterclockwise" width="20" height="20" fill="currentColor"></i>
              </button>
				</a>
			<br>
			<br>
			<div class="container">
			<h3><center>Edit Surat BAST</center></h3>
			<br />
			@include('layouts.messages')
			<br />
			<div class="container">
			<div class="card shadow mb-4" style="background-image: linear-gradient(315deg, #f7b42c 0%, #fc575e 74%); width: 90%; margin: auto;">
            <div class="card-body" style="width: 100%;">
            	@foreach ($data as $row)
            	<form action="{{ url('/surat/datasurat/updatebast',  $row->id_surat) }}" method="POST" id="editform">
                {!! csrf_field() !!}
                
                <input type="text" name="id_tipe_surat" required class="form-control" value="{{ $row->id_tipe_surat }}" hidden>
                <div class="form-group">
                    <label>Client</label>
                    <select name="id_client" class="form-select" required>
                        @foreach ($clients as $pe)
                            <option value="{{ $pe->id_client }}" {{ old('id_client', $row->id_client) == $pe->id_client ? 'selected' : null}}>{{ $pe->nama_client }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Karyawan</label>
                    <select name="id_karyawan" class="form-select" required>
                        @foreach ($karyawans as $kr)
                            <option value="{{ $kr->id_karyawan }}" {{ old('id_karyawan', $row->id_karyawan) == $kr->id_karyawan ? 'selected' : null}}>{{ $kr->nama_karyawan }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Tanggal Surat</label>
                    <input class="datepicker3"  name="tgl_surat" required="required" value="{{ $row->tgl_surat }}">
                </div>
                <div class="form-group">
                    <label>Deskripsi Atas</label>
                    <textarea type="text" name="deskripsi_atas" class="form-control" id="my-editor2{{ $row->id_surat }}" required="required" cols="30" rows="10">{{ strip_tags($row->deskripsi_atas) }}</textarea>
                </div>
                <div class="form-group">
                    <label>Deskripsi Bawah</label>
                    <textarea type="text" name="deskripsi_bawah" class="form-control" id="my-editor3{{ $row->id_surat }}" required="required" cols="30" rows="10">{{ strip_tags($row->deskripsi_bawah) }}</textarea>
                </div>
                <div class="form-group">
                    <label>Project</label>
                    <select name="id_project" class="form-select" required>
                        @foreach ($projects as $pr)
                        <option value="{{ $pr->id_project }}" {{ old('id_project', $row->id_project) == $pr->id_project ? 'selected' : null}}>{{ $pr->nama_project }}</option>
                        @endforeach
                    </select>
                </div>
                <br />
                <div class="modal-footer">
                <center><button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button></center>
                <center><button type="submit" class="btn btn-primary">Simpan</button></center>
                </div>
            </form>
            @endforeach

            <br>
			</div>
			</div>
			</div>

			</div>
			</div>
		</div>
	</div>
</div>

<script>
    $('.datepicker3').datepicker({
        format: "dd-mm-yyyy",
        uiLibrary: 'bootstrap4'
    });
</script>


@endsection

@push('scripts')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('my-editor');
    </script>
<script>
    CKEDITOR.replace('my-editor1');
</script>
@endpush