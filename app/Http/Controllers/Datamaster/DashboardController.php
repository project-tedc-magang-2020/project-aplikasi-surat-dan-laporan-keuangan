<?php

namespace App\Http\Controllers\Datamaster;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $clients = DB::table('m_client')->get();
        $jabatans = DB::table('m_jabatan')->get();
        $karyawans = DB::table('m_karyawan')->get();
        $tipesurats = DB::table('m_tipe_surat')->get();
        
        return view('datamaster.dashboard', compact('clients', 'jabatans', 'karyawans', 'tipesurats'));
    }
}
