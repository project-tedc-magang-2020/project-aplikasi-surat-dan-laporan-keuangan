<?php

namespace App\Http\Controllers\Datamaster;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Tipesurat;

class TipeSuratController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        if($request->has('search')){
            $tipesurats = DB::table('m_tipe_surat')->where('nama_tipe_surat', 'LIKE', '%' .$request->search.'%');
        }else{
            $tipesurats = DB::table('m_tipe_surat');
        }
        $tipesurats = $tipesurats->get();

        $q = DB::table('m_tipe_surat')->select(DB::raw('MAX(RIGHT(id_tipe_surat,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('datamaster.tipesurat.tipesurat', compact('tipesurats', 'kd'));
    }

    public function create()
    {

        return view('datamaster.tipesurat.tambahdatatipesurat');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_tipe_surat' => 'required',
            'nama_tipe_surat' => 'required',
            'template_surat' => 'required'
        ]);

        $tipesurat = Tipesurat::create([
        'id_tipe_surat' => $request->id_tipe_surat,
        'nama_tipe_surat' => $request->nama_tipe_surat,
        'template_surat' => $request->template_surat
        ]);

        if($tipesurat){
        //redirect dengan pesan sukses
            return redirect()->route('datamaster.tipesurat')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('datamaster.tambahdatatipesurat')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_tipe_surat)
    {
        $tipesurats = Tipesurat::where('id_tipe_surat',$id_tipe_surat)->get();

        return view('datamaster.tipesurat.editdatatipesurat', compact('tipesurats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('m_tipe_surat')->where('id_tipe_surat',$request->id_tipe_surat)->update([
	    'nama_tipe_surat' => $request->nama_tipe_surat,
        'template_surat' => $request->template_surat
        ]);

        return redirect('/datamaster/tipesurat')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_tipe_surat)
    {
        DB::table('m_tipe_surat')->where('id_tipe_surat', $id_tipe_surat)->delete();

        return redirect ('/datamaster/tipesurat')->with(['success' => 'Data Deleted Successfully!']);
    }
}
