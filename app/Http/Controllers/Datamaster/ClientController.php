<?php

namespace App\Http\Controllers\Datamaster;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Client;

class ClientController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $clients = DB::table('m_client')->where('nama_client', 'LIKE', '%' .$request->search.'%');
        }else{
            $clients = DB::table('m_client');
        }
        $clients = $clients->get();

        $q = DB::table('m_client')->select(DB::raw('MAX(RIGHT(nomor_client,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('datamaster.client.client', compact('clients', 'kd'));
    }

    public function create()
    {

        $q = DB::table('m_client')->select(DB::raw('MAX(RIGHT(nomor_client,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }
        return view('datamaster.client.createclient',compact('kd'));
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'nomor_client' => 'required',
            'nama_client' => 'required',
            'alamat' => 'required',
            'telepon' => 'required',
            'email' => 'required',
            'jabatan' => 'required',
            'perusahaan' => 'required',
            'penanggung_jawab1' => 'required',
            'penanggung_jawab_jabatan1' => 'required',
            'penanggung_jawab2' => 'required',
            'penanggung_jawab_jabatan2' => 'required',
            'penanggung_jawab3' => 'required',
            'penanggung_jawab_jabatan3' => 'required'
        ]);

        $client = Client::create([
        'id_client' => $request->id_client,
        'nomor_client' => $request->nomor_client,
        'nama_client' => $request->nama_client,
        'alamat' => $request->alamat,
        'telepon' => $request->telepon,
        'email' => $request->email,
        'jabatan' => $request->jabatan,
        'perusahaan' => $request->perusahaan,
        'penanggung_jawab1' => $request->penanggung_jawab1,
        'penanggung_jawab_jabatan1' => $request->penanggung_jawab_jabatan1,
        'penanggung_jawab2' => $request->penanggung_jawab2,
        'penanggung_jawab_jabatan2' => $request->penanggung_jawab_jabatan2,
        'penanggung_jawab3' => $request->penanggung_jawab3,
        'penanggung_jawab_jabatan3' => $request->penanggung_jawab_jabatan3
        
        ]);

        if($client){
        //redirect dengan pesan sukses
            return redirect()->route('datamaster.client')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('datamaster.createclient')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_client)
    {
        $clients = Client::where('id_client',$id_client)->get();

        return view('datamaster.client.editclient', compact('clients'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('m_client')->where('id_client',$request->id_client)->update([
            'nomor_client' => $request->nomor_client,
            'nama_client' => $request->nama_client,
            'alamat' => $request->alamat,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'jabatan' => $request->jabatan,
            'perusahaan' => $request->perusahaan,
            'penanggung_jawab1' => $request->penanggung_jawab1,
            'penanggung_jawab_jabatan1' => $request->penanggung_jawab_jabatan1,
            'penanggung_jawab2' => $request->penanggung_jawab2,
            'penanggung_jawab_jabatan2' => $request->penanggung_jawab_jabatan2,
            'penanggung_jawab3' => $request->penanggung_jawab3,
            'penanggung_jawab_jabatan3' => $request->penanggung_jawab_jabatan3
        ]);

        return redirect('/datamaster/client')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_client)
    {
        DB::table('m_client')->where('id_client', $id_client)->delete();

        return redirect ('/datamaster/client')->with(['success' => 'Data Deleted Successfully!']);
    }

    // public function show(Request $request,$id_client)
    // {
    //     $cli = Client::find($id_client);
    //     return view('datamaster.client.showclient',compact('clients'))->renderSections()['content'];
    // }
}
