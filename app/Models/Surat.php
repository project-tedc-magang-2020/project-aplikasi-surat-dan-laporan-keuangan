<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Surat extends Model
{
    public $table = "t_surat";
    protected $primaryKey = 'id_surat';
    public $timestamps = false;
    public $incrementing = false;

    protected $fillable = [
        'id_surat', 'id_tipe_surat', 'id_client','id_karyawan','nomor_surat','tgl_surat','perihal_surat','deskripsi_atas','deskripsi_bawah','status','status_reviewer','id_project_include',
    ];
}

