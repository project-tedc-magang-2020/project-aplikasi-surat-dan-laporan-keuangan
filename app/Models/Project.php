<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Project extends Model
{
    public $table = 't_project';
    protected $primaryKey = 'id_project';
    public $timestamps = false;

    protected $fillable = [
        'id_project', 'id_client', 'nomor_project','nama_project','tgl_mulai','tgl_selesai','nilai_project'
    ];
}
