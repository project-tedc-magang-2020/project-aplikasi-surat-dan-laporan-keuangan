<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Iteminvoices extends Model
{
    public $table = 't_item_invoice';
    protected $primaryKey = 'id_item';
    public $timestamps = false;

    protected $fillable = [
        'id_item', 'id_surat', 'nama_item', 'harga_item',
    ];
}
