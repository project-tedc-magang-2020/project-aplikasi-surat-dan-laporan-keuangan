<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public $table = "m_client";
    protected $primaryKey = 'id_client';
    public $timestamps = false;
   

    protected $fillable = [
        'id_client', 'nomor_client', 'nama_client','alamat','telepon','email', 'jabatan', 'perusahaan','penanggung_jawab1','penanggung_jawab_jabatan1','penanggung_jawab2','penanggung_jawab_jabatan2','penanggung_jawab3','penanggung_jawab_jabatan3',
    ];
}

