<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tipesurat extends Model
{
    public $table = 'm_tipe_surat';
    protected $primaryKey = 'id_tipe_surat';
    public $timestamps = false;

    protected $fillable = [
        'id_tipe_surat', 'nama_tipe_surat', 'template_surat'
    ];
}
