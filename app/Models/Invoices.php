<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Invoices extends Model
{
    public $table = 't_invoice';
    protected $primaryKey = 'id_invoice';
    public $timestamps = false;

    protected $fillable = [
        'id_invoice', 'id_surat', 'total_harga',
    ];
}
